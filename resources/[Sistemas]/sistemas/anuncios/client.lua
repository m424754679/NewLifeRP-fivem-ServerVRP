------------------[ CHANGE THIS ]--------------------
local minutesBetweenAnnouncements = 15
local prefix = "^0[Noticias New Life]^0"
local suffix = "^0."
local messages = {
    '^1Sejam Bem Vindos ao New Life RP, o servidor ainda está em Beta,então pode haver Bugs',
    '^1Achou um BUG ou algo do Genero? Avise a um Staff Voce pode Ganhar Recompensas',
    '^1Ajude o Servidor a Ficar Online, Faça sua Doação.',
    '^1Mais Novidades serão Adicionadas no Servidor Constantemente',
    '^1Evite Brigas e Discussões e Sigas as Regras do Servidor, Jogadores que desobedecerem as Regras, Tomarão Banimento Permanente.',
    '^1Para sua Segurança e a Segurança de todos, seu IP ficara Gravado no Servidor'
}
-----------------------------------------------------

--  Extra info:
--  You can use ^0-^9 in your messages to change text color.
--  You only need to change the messages above,
--  the code below shouldn't be touched.
--  The script runs only on the client side so the server won't
--  have any extra load.
-- 
















-------[ CODE, NO NEED TO TOUCH THIS PART! ]---------
local count = 0
for _ in pairs(messages) do count = count + 1 end

local timeout = minutesBetweenAnnouncements * 60000
local i = 1
Citizen.CreateThread(function()
    while true do
        TriggerEvent('chatMessage', '', { 255, 255, 255 }, prefix .. " " .. messages[i] .. suffix)
        i = i + 1
        if (i == (count + 1)) then
            i = 1
        end
        Citizen.Wait(timeout)
    end
end)
-----------------------------------------------------
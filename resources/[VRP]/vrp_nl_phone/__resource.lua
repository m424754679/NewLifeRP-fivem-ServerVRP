
description "New Life Telefone"

dependency "vrp"

server_scripts{
  '@vrp/lib/utils.lua',
  'server.lua'
}

files{
  "gui/font/bankgothic.ttf",
  "gui/img/menu.png",
  "gui/img/border.png",
  "gui/img/blue.jpg",
  "gui/img/code.jpg",
  "gui/img/grey.jpg",
  "gui/img/red.jpg",
  "gui/img/red2.jpg"     
}

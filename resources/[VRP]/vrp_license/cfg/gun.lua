cfg = {}

cfg.lang = "en"

cfg.gun = {
  warn = true,
  peds = {
    {type=4, hash=0xc99f21c4, x=450.96667480469, y=-977.23480224609, z=29.889584732056, h=3374176}
  },
  price = {
    practical = 12000
  },
  blip = {
    id = 110,
    colour = 1,
	size = 0.8,
    title = "Porte e Posse de Arma"
  },
  menu = {
    x = 0.1,
    y = 0.2,
    width = 0.2,
    height = 0.04,
    scale = 0.4,
    font = 0,
    menu_title = "Comprar porte e posse de arma",
    menu_subtitle = "Porte e Posse de arma",
    color_r = 255,
    color_g = 0,
    color_b = 0,
  }
}

return cfg
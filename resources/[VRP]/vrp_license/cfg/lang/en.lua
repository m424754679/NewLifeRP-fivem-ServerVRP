local lang = {
  dmv = {
    police = {
	  check = "Checar porte de arma",
	  take = "Suspender porte de arma",
	  perm_ask = "police.ask_firearms",
	  perm_take = "police.take_license",
	  check_desc = "Checar porte de arma do player proximo.",
	  take_desc = "Suspender Porte do jogador proximo.",
	  ask = "Pedir Porte...",
	  request = "Voce gostaria de mostrar sua Porte ?",
	  request_hide = "Esconder sua Porte.",
	  confirm = "Você realmente quer tirar essa Porte?",
	  license = "<em>Nome: </em>{1}<br /><em>Primeiro Nome: </em>{2}<br /><em>Idade: </em>{3}<br /><em>Numero de Registro: </em>{4}<br /><em>Telefone: </em>{5}<br /><em>Data: </em>{6}<br /><em>Porte de Armas: OK </em><br />",
	  no_license = "~r~Esse cidadão não tem Porte!",
	  took_license = "~g~A Porte foi removido.",
	  license_taken = "~r~Sua Porte foi removido!"
    },
  },
  client = {
	buy_success = "~g~Você compro uma porte",
	interact = "Pressione ~INPUT_CONTEXT~ para compra ~y~ um porte de Armas",
	menu = {
	  obtain = "Porte de Armas",
	  practical = "Compra porte de armas R$12000",
	  mreturn = "Retornar",
	  mclose = "Fechar",
	},
  },
}

return lang
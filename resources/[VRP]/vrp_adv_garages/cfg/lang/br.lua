
local lang = {
  garage = {
    buy = {
	  item = "{2} {1}<br /><br />{3}",
	  request = "Tem certeza que deseja comprar este veículo?",
	},
    keys = {
	  title = "Chaves",
	  key = "chave ({1})",
	  description = "Verifique as chaves do seu veículo",
	  sell = {
	    title = "Vender",
		prompt = "Valor de oferta:",
	    owned = "Você já possui este veículo",
		request = "Você aceita a oferta de um {1} por R${2}?",
		description = "Oferte um veículo a um jogador próximo."
	  }
	},
    personal = {
	  client = {
	    locked = "Portas do veículo trancadas",
		unlocked = "Portas do veículo destrancadas"
	  },
	  out = "Você já possui um veículo deste tipo fora da garagem",
	  bought = "Veículo adquirido e enviado para a garagem",
	  sold = "Veículo vendido e retirado de sua garagem",
	  stored = "Veículo guardado",
	  toofar = "O veículo está longe de mais"	  
	},
	showroom = {
	  title = "Concessionária",
	  description = "Clique com o botão direito para ver uma prévia e pressione ENTER para comprar"
	},
    shop = {
	  title = "Oficina",
	  description = "Utilize o scroll para ver as modificações",
	  client = {
	    nomods = "~r~Nenhuma modificação deste tipo para este veículo",
		maximum = "Você atingiu o valor ~y~máximo~w~ para esta modificação",
		minimum = "Você atingiu o valor ~r~mínimo~w~ para esta modificação",
	    toggle = {
		  applied = "~g~Modificação aplicada",
		  removed = "~r~Modificação removida"
		}
	  },
	  mods = {
	    title = "Modificações",
		info = "Escolha dentre as modificações disponíveis",
	  },
	  repair = {
	    title = "Reparar",
		info = "Conserte seu veículo",
	  },
	  colour = {
	    title = "Pintura",
		info = "Selecione as cores do veículo",
		primary = "Cor primária",
		secondary = "Cor secundária",
	    extra = {
		  title = "Cor extra",
		  info = "Escolha dentre as cores extras",
	      pearlescent = "Pearlescent Color",
	      wheel = "Cor da roda",
	      smoke = "Cor da fumaça",
		},
		custom = {
		  primary = "Cor primária customizada",
		  secondary = "Cor secundária customizada",
		},
	  },
	  neon = {
	    title = "Luzes neon",
		info = "Modifique as luzes neon de seu veículo",
	    front = "Neon frontal",
	    back = "Neon traseiro",
	    left = "Neon esquerdo",
	    right = "Neon direito",
	    colour = "Cor do neon"
	  }
	}
  }
}

return lang

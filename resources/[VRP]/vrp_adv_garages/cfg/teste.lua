
local cfg = {}
-- define garage types with their associated vehicles
-- (vehicle list: https://wiki.fivem.net/wiki/Vehicles)

-- each garage type is an associated list of veh_name/veh_definition 
-- they need a _config property to define the blip and the vehicle type for the garage (each vtype allow one vehicle to be spawned at a time, the default vtype is "default")
-- this is used to let the player spawn a boat AND a car at the same time for example, and only despawn it in the correct garage
-- _config: gtype, vtype, blipid, blipcolor, ghome, permissions (optional, only users with the permission will have access to the shop)
-- vtype: identifies the "type" of vehicle for the personal garages and vehicles (you can create new ones)
-- gtype: there are 5 gtypes> personal, showroom, shop, store and rental (you cant create new ones, one garage can have many gtypes)
   -- personal: allow you to get any personal vehicle of the same vtype of the garage
   -- showroom: allows you to see the vehicle model before purchasing it
   -- shop: allows you to modify your vehicle
   -- store: allows you to purchase and sell vehicles
   -- rental: allows you to rent vehicles for that session for a part of the price
-- ghome: links the garage with an address, only owners of that address will have see the garage
-- gpay: bank or wallet
-- Car/Mod: [id/model] = {"Display Name", price/amount, "", (optional) item}, -- when charging items, price becomes amount

cfg.lang = "br" -- lenguage file

cfg.rent_factor = 0.1 -- 10% of the original price if a rent
cfg.sell_factor = 0.75 -- sell for 75% of the original price

cfg.price = {
  repair = 500, -- value to repair the vehicle
  colour = 100, -- value will be charged 3 times for RGB
  extra = 100, -- value will be charged 3 times for RGB
  neon = 100 -- value will be charged 3 times for RGB
}

-- declare any item used on purchase that doesnt exist yet (name,description,choices,weight}
cfg.items = {
  ["issi2key"] = {"Issi 2 Key","Buys an Issi",nil,0.5} -- example
}

-- configure garage types
cfg.adv_garages = {

  ["Barco de Pesca"] = {
    _config = {gpay="wallet",gtype={"rental"},vtype="barcopesca",blipid=427,blipcolor=28,permissions={"garagem.pescador"}},
    ["suntrap"] = {"Barco de Pesca",0, "O melhor basco de pesca!"}
  },

  ["Transporte de Valores"] = {
    _config = {gpay="wallet",gtype={"rental"},vtype="carroforte",blipid=67,blipcolor=4,permissions={"garagem.carroforte"}},
    ["stockade"] = {"Carro Forte",0, ""}
  },

  ["Caminhoneiro"] = {
    _config = {gpay="wallet",gtype={"rental"},vtype="caminhoneiro",blipid=67,blipcolor=4,permissions={"garagem.caminheiro"}},
    ["daf"] = {"Daf XF 105",381000, ""}
  },

  ["Coronel Polícia Militar"] = {
    _config = {gpay="wallet",gtype={"rental"},vtype="pmcoronel",blipid=50,blipcolor=4,permissions={"garagem.pmcoronel"}},
    ["rotasuburban"] = {"Suburban Descaracterizada",0, ""},
    ["pmcobalt"] = {"Cobalt PM",0, ""},
    ["pmfocus"] = {"Focus PM",0, ""},
    ["pmpalio"] = {"Palio PM",0, ""},
    ["pmpassat"] = {"Passat PM - Em teste",0, ""},
    ["pmvan"] = {"Van PM - Em teste",0, ""},
    ["trailft"] = {"Trail Blazer Força Tatica",0, ""},
    ["blazerft"] = {"Blazer Força Tatica",0, ""},
    ["ftsw4"] = {"SW4 Força Tatica - Testes",0, ""}
  },

  ["Polícia Rodoviária Federal"] = {
    _config = {gpay="wallet",gtype={"rental"},vtype="prf",blipid=50,blipcolor=4,permissions={"garagem.prf"}},
    ["blazerprf"] = {"Blazer PRF",0, ""},
    ["dakarprf"] = {"Dakar PRF",0, ""},
    ["nissanprf"] = {"Nissan PRF",0, ""}
  },

  ["Polícia Militar"] = {
    _config = {gpay="wallet",gtype={"rental"},vtype="pm",blipid=50,blipcolor=4,permissions={"garagem.pm"}},
    ["pmcobalt"] = {"Cobalt PM",0, ""},
    ["pmfocus"] = {"Focus PM",0, ""},
    ["pmpalio"] = {"Palio PM",0, ""},
    ["pmpassat"] = {"Passat PM - Em teste",0, ""},
    ["pmvan"] = {"Van PM - Em teste",0, ""},
    ["ftsw4"] = {"SW4 Força Tatica - Testes",0, ""}
  },

  ["Força Tática Polícia Militar"] = {
    _config = {gpay="wallet",gtype={"rental"},vtype="pmft",blipid=50,blipcolor=4,permissions={"garagem.pmft"}},
    ["trailft"] = {"Trail Blazer Força Tatica",0, ""},
    ["blazerft"] = {"Blazer Força Tatica",0, ""}
  },

  ["ROCAM Polícia Militar"] = {
    _config = {gpay="wallet",gtype={"rental"},vtype="rocam",blipid=50,blipcolor=4,permissions={"garagem.rocam"}},
     ["tigerrocam"] = {"Tigger Rocam",0, ""},
     ["pmcobalt"] = {"Cobalt PM",0, ""},
     ["pmpalio"] = {"Palio PM",0, ""}
  },

  ["Águia Polícia Militar"] = {
    _config = {gpay="wallet",gtype={"rental"},vtype="pmaguia",blipid=50,blipcolor=4,permissions={"garagem.aguia"}},
     ["as350"] = {"Aguia",0, ""},
     ["pmcobalt"] = {"Cobalt PM",0, ""},
     ["pmpalio"] = {"Palio PM",0, ""}
  },

  ["Coronel ROTA"] = {
    _config = {gpay="wallet",gtype={"rental"},vtype="rotacoronel",blipid=50,blipcolor=4,permissions={"garagem.rotacoronel"}},
    ["rotatahoe"] = {"Rota Tahoe",0, ""}
  },

  ["ROTA"] = {
    _config = {gpay="wallet",gtype={"rental"},vtype="rota",blipid=50,blipcolor=4,permissions={"garagem.rota"}},
    ["rotatahoe"] = {"Rota Tahoe",0, ""},
    ["rotasw4"] = {"SW4 Rota - Testes",0, ""}
  },

  ["Helicóptero ROTA"] = {
    _config = {gpay="wallet",gtype={"rental"},vtype="rotaheli",blipid=50,blipcolor=4,permissions={"garagem.rotaheli"}},
    ["valkyrie2"] = {"Helicoptero da Rota",0, ""},
  },

  ["Polícia Civil"] = {
    _config = {gpay="wallet",gtype={"rental"},vtype="pcivil",blipid=50,blipcolor=4,permissions={"garagem.pc"}},
    ["blazerpc"] = {"Blazer Policia Civil",0, ""},
    ["trailpc"] = {"Trail Blazer Policia Civil",0, ""},
    ["descpc"] = {"Descaracterizada Policia Civil",0, ""},
    ["garrasw4"] = {"Garra Policia Civil",0, ""}
  },

  ["Pelicano Polícia Civil"] = {
    _config = {gpay="wallet",gtype={"rental"},vtype="pcheli",blipid=50,blipcolor=4,permissions={"garagem.pcheli"}},
    ["pcheli"] = {"Pelicano",0, ""}
  },

  ["SAMU"] = {
    _config = {gpay="wallet",gtype={"rental"},vtype="samu",blipid=50,blipcolor=4,permissions={"garagem.hospital"}},
    ["mastersamu"] = {"Ambulancia",0, ""}
  },

  ["Bombeiros"] = {
    _config = {gpay="wallet",gtype={"rental"},vtype="bombeiros",blipid=50,blipcolor=4,permissions={"garagem.bombeiros"}},
    ["mastersamu"] = {"Ambulancia",0, ""}
  },

  ["Garagem Globo News"] = {
    _config = {gpay="wallet",gtype={"rental"},vtype="globonews",blipid=50,blipcolor=4,permissions={"garagem.globonews"}},
    ["speedo3"] = {"Van da Globo",0, ""}
  },

  ["uber"] = {
    _config = {gpay="wallet",gtype={"rental"},vtype="uber",blipid=198,blipcolor=5,permissions={"garagem.uber"}},
    ["fusiont"] = {"Uber",0, ""}
  },

  ["Mecanico"] = {
    _config = {gpay="wallet",gtype={"rental"},vtype="mecanico",blipid=85,blipcolor=31,permissions={"garagem.mecanico"}},
    ["towtruck2"] = {"towtruck2",0, ""}
  },
  
  ["Entregador"] = {
    _config = {gpay="wallet",gtype={"rental"},vtype="entregador",blipid=355,blipcolor=4,permissions={"garagem.entregador"}},
    ["enduro"] = {"Fan 150",8000, ""}
  },
  ["Barcos"]  = {
    _config = {gpay="bank",gtype={"showroom","store","rental"},vtype="barco",blipid=471,blipcolor=5},
    ["seashark"] = {"Jet ski",80000, ""},
    ["seashark2"] = {"Jet ski 2.0",80000, ""},
    ["tropic"] = {"Tropic",100000, ""},
    ["jetmax"] = {"Jetmax",120000, ""},
    ["yacht4"] = {"Yacht ",50000000, ""},
    ["tropic"] = {"Tropic",100000, ""},
    ["jetmax"] = {"Jetmax",120000, ""},
    ["submersible2"] = {"Kraken",1000000, ""}
  },

  ["Veículos Aéreos"]  = {
    _config = {gpay="bank",gtype={"showroom","store"},vtype="aereo",blipid=43,blipcolor=5},
    ["luxor"] = {"Luxor",2600000, ""},
    ["shamal"] = {"Shamal",2600000, ""},
    ["volatus"] = {"Volatus",1150000, ""}
  },


  ["Compactos"]  = {
    _config = {gpay="bank",gtype={"showroom","store","rental"},vtype="car",blipid=50,blipcolor=4},
    ["blista"] = {"Blista", 15000, ""},
    ["brioso"] = {"Brioso R/A", 155000, ""},
    ["dilettante"] = {"Dilettante", 25000, ""},
    ["panto"] = {"Panto", 85000, ""},
    ["prairie"] = {"Prairie", 30000, ""},
    ["rhapsody"] = {"Rhapsody", 120000, ""},
    ["veloster"] = {"Hyundai Veloster",44000, ""}
  },

  ["Coupe"] = {
    _config = {gpay="bank",gtype={"showroom","store","rental"},vtype="car",blipid=50,blipcolor=4},
    ["cogcabrio"] = {"Cognoscenti Cabrio",180000, ""},
    ["exemplar"] = {"Exemplar", 200000, ""},
    ["f620"] = {"F620", 80000, ""},
    ["felon"] = {"Felon", 90000, ""},
    ["felon2"] = {"Felon GT", 95000, ""},
    ["jackal"] = {"Jackal", 60000, ""},
    ["oracle"] = {"Oracle", 80000, ""},
    ["oracle2"] = {"Oracle XS",82000, ""},
    ["sentinel"] = {"sentinel", 90000, ""},
    ["sentinel2"] = {"Sentinel XS", 60000, ""},
    ["windsor"] = {"Windsor",800000, ""},
    ["windsor2"] = {"Windsor Drop",850000, ""},
    ["zion"] = {"Zion", 60000, ""},
    ["zion2"] = {"Zion Cabrio", 65000, ""}
  },

  ["Esportivos"] = {
    _config = {gpay="bank",gtype={"showroom","store","rental"},vtype="car",blipid=50,blipcolor=4},
    ["ninef"] = {"9F",120000, ""},
    ["ninef2"] = {"9F Cabrio",130000, ""},
    ["alpha"] = {"Alpha",150000, ""},
    ["banshee"] = {"Banshee",105000, ""},
    ["bestiagts"] = {"Bestia GTS",610000, ""},
    ["blista"] = {"Blista Compact",42000, ""},
    ["buffalo"] = {"Buffalo",35000, ""},
    ["buffalo2"] = {"Buffalo S",96000, ""},
    ["carbonizzare"] = {"Carbonizzare",195000, ""},
    ["comet2"] = {"Comet",100000, ""},
    ["coquette"] = {"Coquette",138000, ""},
    ["tampa2"] = {"Drift Tampa",995000, ""},
    ["feltzer2"] = {"Feltzer",130000, ""},
    ["furoregt"] = {"Furore GT",448000, ""},
    ["fusilade"] = {"Fusilade",36000, ""},
    ["jester"] = {"Jester",240000, ""},
    ["jester2"] = {"Jester (Racecar)",350000, ""},
    ["kuruma"] = {"Kuruma",95000, ""},
    ["lynx"] = {"Lynx",1735000, ""},
    ["massacro"] = {"Massacro",275000, ""},
    ["massacro2"] = {"Massacro (Racecar)",385000, ""},
    ["omnis"] = {"Omnis",701000, ""},
    ["penumbra"] = {"Penumbra",24000, ""},
    ["rapidgt"] = {"Rapid GT",140000, ""},
    ["rapidgt2"] = {"Rapid GT Convertible",150000, ""},
    ["schafter3"] = {"Schafter V12",140000, ""},
    ["sultan"] = {"Sultan",12000, ""},
    ["surano"] = {"Surano",110000, ""},
    ["tropos"] = {"Tropos",816000, ""},
    ["verlierer2"] = {"Verkierer",695000,""}
  },

  ["Classicos"] = {
    _config = {gpay="bank",gtype={"showroom","store","rental"},vtype="car",blipid=50,blipcolor=4},
    ["casco"] = {"Casco",680000, ""},
    ["coquette2"] = {"Coquette Classic",665000, ""},
    ["jb700"] = {"JB 700",350000, ""},
    ["pigalle"] = {"Pigalle",400000, ""},
    ["stinger"] = {"Stinger",850000, ""},
    ["stingergt"] = {"Stinger GT",875000, ""},
    ["feltzer3"] = {"Stirling",975000, ""},
    ["ztype"] = {"Z-Type",950000,""}
  },

  ["Super Esportivos"] = {
    _config = {gpay="bank",gtype={"showroom","store","rental"},vtype="car",blipid=50,blipcolor=4},
    ["adder"] = {"Adder",1000000, ""},
    ["banshee2"] = {"Banshee 900R",565000, ""},
    ["bullet"] = {"Bullet",155000, ""},
    ["cheetah"] = {"Cheetah",650000, ""},
    ["entityxf"] = {"Entity XF",795000, ""},
    ["sheava"] = {"ETR1",199500, "4 - (smaller number better car"},
    ["fmj"] = {"FMJ",1750000, "10 - (smaller number better car"},
    ["infernus"] = {"Infernus",440000, ""},
    ["osiris"] = {"Osiris",1950000, "8 - (smaller number better car"},
    ["le7b"] = {"RE-7B",5075000, "1 - (smaller number better car"},
    ["reaper"] = {"Reaper",1595000, ""},
    ["sultanrs"] = {"Sultan RS",795000, ""},
    ["t20"] = {"T20",2200000,"7 - (smaller number better car"},
    ["turismor"] = {"Turismo R",500000, "9 - (smaller number better car"},
    ["tyrus"] = {"Tyrus",2550000, "5 - (smaller number better car"},
    ["vacca"] = {"Vacca",240000, ""},
    ["voltic"] = {"Voltic",150000, ""},
    ["prototipo"] = {"X80 Proto",2700000, "6 - (smaller number better car"},
    ["zentorno"] = {"Zentorno",725000,"3 - (smaller number better car"}
  },

  ["Carros de Alta Potencia"] = {
    _config = {gpay="bank",gtype={"showroom","store","rental"},vtype="car",blipid=50,blipcolor=4},
    ["blade"] = {"Blade",160000, ""},
    ["buccaneer"] = {"Buccaneer",29000, ""},
    ["Chino"] = {"Chino",225000, ""},
    ["coquette3"] = {"Coquette BlackFin",695000, ""},
    ["dominator"] = {"Dominator",35000, ""},
    ["dukes"] = {"Dukes",62000, ""},
    ["gauntlet"] = {"Gauntlet",32000, ""},
    ["hotknife"] = {"Hotknife",90000, ""},
    ["faction"] = {"Faction",36000, ""},
    ["nightshade"] = {"Nightshade",585000, ""},
    ["picador"] = {"Picador",9000, ""},
    ["sabregt"] = {"Sabre Turbo",15000, ""},
    ["tampa"] = {"Tampa",375000, ""},
    ["virgo"] = {"Virgo",195000, ""},
    ["vigero"] = {"Vigero",21000, ""}
  },

  ["Off-Road"] = {
    _config = {gpay="bank",gtype={"showroom","store","rental"},vtype="car",blipid=50,blipcolor=4},
    ["bifta"] = {"Bifta",75000, ""},
    ["blazer"] = {"Blazer",8000, ""},
    ["brawler"] = {"Brawler",715000, ""},
    ["dubsta3"] = {"Bubsta 6x6",249000, ""},
    ["dune"] = {"Dune Buggy",20000, ""},
    ["rebel2"] = {"Rebel",22000, ""},
    ["sandking"] = {"Sandking",38000, ""},
    ["monster"] = {"The Liberator",550000, ""},
    ["trophytruck"] = {"Trophy Truck",550000, ""}
  },

  ["Utilitarios"]  = {
    _config = {gpay="bank",gtype={"showroom","store","rental"},vtype="car",blipid=50,blipcolor=4},
    ["baller"] = {"Baller",90000, ""},
    ["cavalcade"] = {"Cavalcade",60000, ""},
    ["granger"] = {"Grabger",35000, ""},
    ["huntley"] = {"Huntley",195000, ""},
    ["landstalker"] = {"Landstalker",58000, ""},
    ["radi"] = {"Radius",32000, ""},
    ["rocoto"] = {"Rocoto",85000, ""},
    ["seminole"] = {"Seminole",30000, ""},
    ["xls"] = {"XLS",253000, ""}
  },

  ["Vans"] = {
    _config = {gpay="bank",gtype={"showroom","store","rental"},vtype="car",blipid=50,blipcolor=4},
    ["bison"] = {"Bison",30000, ""},
    ["bobcatxl"] = {"Bobcat XL",23000, ""},
    ["gburrito"] = {"Gang Burrito",65000, ""},
    ["journey"] = {"Journey",15000, ""},
    ["minivan"] = {"Minivan",30000, ""},
    ["paradise"] = {"Paradise",25000, ""},
    ["rumpo"] = {"Rumpo",13000, ""},
    ["surfer"] = {"Surfer",11000, ""},
    ["youga"] = {"Youga",16000, ""}
  },

  ["Sedans"] = {
    _config = {gpay="bank",gtype={"showroom","store","rental"},vtype="car",blipid=50,blipcolor=4},
    ["asea"] = {"Asea",1000000, ""},
    ["asterope"] = {"Asterope",1000000, ""},
    ["cognoscenti"] = {"Cognoscenti",1000000, ""},
    ["cognoscenti2"] = {"Cognoscenti(Armored)",1000000, ""},
    ["cog55"] = {"Cognoscenti 55",1000000, ""},
    ["cog552"] = {"Cognoscenti 55(Armored)",1500000, ""},
    ["fugitive"] = {"Fugitive",24000, ""},
    ["glendale"] = {"Glendale",200000, ""},
    ["ingot"] = {"Ingot",9000, ""},
    ["intruder"] = {"Intruder",16000, ""},
    ["premier"] = {"Premier",10000, ""},
    ["primo"] = {"Primo",9000, ""},
    ["primo2"] = {"Primo Custom",9500, ""},
    ["regina"] = {"Regina",8000, ""},
    ["schafter2"] = {"Schafter",65000, ""},
    ["stanier"] = {"Stanier",10000, ""},
    ["stratum"] = {"Stratum",10000, ""},
    ["stretch"] = {"Stretch",30000, ""},
    ["superd"] = {"Super Diamond",250000, ""},
    ["surge"] = {"Surge",38000, ""},
    ["tailgater"] = {"Tailgater",55000, ""},
    ["warrener"] = {"Warrener",120000, ""},
    ["washington"] = {"Washington",15000, ""}
  },
  
  ["Motos"] = {
    _config = {gpay="bank",gtype={"showroom","store","rental"},vtype="car",blipid=50,blipcolor=4},
    ["bmws"] = {"BMW S1000RR",76000, ""},
    ["nh2r"] = {"Kawasaki Ninja H2R",350000, ""},
    ["zx10r"] = {"Kawasaki ZX10R",75000, ""},
    ["rc"] = {"KTM RC390",23000, ""},
    ["dm1200"] = {"Ducati Multilistrada",65000, ""},
    ["d99"] = {"Ducati 1199 Panigale"},
    ["hcbr17"] = {"Honda CBR 1000RR",85000, ""},
    ["titan60"] = {"Honda CG Titan 160 EX",12000, ""},
    ["mt10"] = {"Yamaha MT10",58000, ""},
    ["r1v2"] = {"Yamaha YZF R1",125000, ""},
    ["r6"] = {"Yamaha R6",43000, ""},
    ["xt66"] = {"Yamaha XT660R",34000, ""},
    ["mt09"] = {"Yamaha MT09",50000, ""},
    ["AKUMA"] = {"Akuma",9000, ""},
    ["bagger"] = {"Bagger",5000, ""},
    ["bati"] = {"Bati 801",15000, ""},
    ["bati2"] = {"Bati 801RR",15000, ""},
    ["bf400"] = {"BF400",95000, ""},
    ["carbonrs"] = {"Carbon RS",40000, ""},
    ["cliffhanger"] = {"Cliffhanger",225000, ""},
    ["daemon"] = {"Daemon",5000, ""},
    ["double"] = {"Double T",12000, ""},
    ["enduro"] = {"Enduro",48000, ""},
    ["faggio2"] = {"Faggio",4000, ""},
    ["gargoyle"] = {"Gargoyle",120000, ""},
    ["hakuchou"] = {"Hakuchou",82000, ""},
    ["hexer"] = {"Hexer",15000, ""},
    ["innovation"] = {"Innovation",90000, ""},
    ["lectro"] = {"Lectro",700000, ""},
    ["nemesis"] = {"Nemesis",12000, ""},
    ["pcj"] = {"PCJ-600",9000, ""},
    ["ruffian"] = {"Ruffian",9000, ""},
    ["sanchez"] = {"Sanchez",7000, ""},
    ["sovereign"] = {"Sovereign",90000, ""},
    ["thrust"] = {"Thrust",75000, ""},
    ["vader"] = {"Vader",9000, ""},
    ["vindicator"] = {"Vindicator",600000,""}
  },

  ["Populares"] = {
    _config = {gpay="bank",gtype={"showroom","store","rental"},vtype="car",blipid=50,blipcolor=4},
    ["fulux63"] = {"Volkswagen Fusca",10000, ""},
    ["chevette"] = {"Chevrolet Chevette",17000, ""},
    ["citroc4"] = {"Citroen C4",45000, ""},
    ["206"] = {"Peugeot 206",15000, ""},
    ["fordka"] = {"Ford KA",10000, ""},
    ["mk7"] = {"Volkswagen Golf",150000, ""},
    ["honda"] = {"Honda Civic Hatchback",87000, ""}
  },
  
  ["Particular"]  = {
    _config = {gpay="wallet",gtype={"personal"},vtype="car",blipid=357,blipcolor=3},
  -- Vehicles of differenty vtype can be added to the personal garage, adding to one means adding to all of the same vtype
    ["AKUMA"] = {"Akuma",9000, ""},
    ["bagger"] = {"Bagger",5000, ""},
    ["bati"] = {"Bati 801",15000, ""},
    ["bati2"] = {"Bati 801RR",15000, ""},
    ["bf400"] = {"BF400",95000, ""},
    ["carbonrs"] = {"Carbon RS",40000, ""},
    ["cliffhanger"] = {"Cliffhanger",225000, ""},
    ["daemon"] = {"Daemon",5000, ""},
    ["double"] = {"Double T",12000, ""},
    ["enduro"] = {"Enduro",48000, ""},
    ["faggio2"] = {"Faggio",4000, ""},
    ["gargoyle"] = {"Gargoyle",120000, ""},
    ["hakuchou"] = {"Hakuchou",82000, ""},
    ["hexer"] = {"Hexer",15000, ""},
    ["innovation"] = {"Innovation",90000, ""},
    ["lectro"] = {"Lectro",700000, ""},
    ["nemesis"] = {"Nemesis",12000, ""},
    ["pcj"] = {"PCJ-600",9000, ""},
    ["ruffian"] = {"Ruffian",9000, ""},
    ["sanchez"] = {"Sanchez",7000, ""},
    ["sovereign"] = {"Sovereign",90000, ""},
    ["thrust"] = {"Thrust",75000, ""},
    ["vader"] = {"Vader",9000, ""},
    ["vindicator"] = {"Vindicator",600000,""}
  },
  
  ["Fazenda"]  = {
    _config = {gpay="wallet",gtype={"personal"},vtype="car",blipid=357,blipcolor=3,ghome="Fazenda"},
  },
  ["Apartamento de luxo"]  = {
    _config = {gpay="wallet",gtype={"personal"},vtype="car",blipid=357,blipcolor=3,ghome="Apartamento de luxo"},
  },
  ["Apartamento Simples 1"]  = {
    _config = {gpay="wallet",gtype={"personal"},vtype="car",blipid=357,blipcolor=3,ghome="Apartamento Simples 1"},
  },
  ["Apartamento Simples 2"]  = {
    _config = {gpay="wallet",gtype={"personal"},vtype="car",blipid=357,blipcolor=3,ghome="Apartamento Simples 2"},
  },
  
  ["LS Customs"]  = {
    _config = {gpay="wallet",gtype={"shop"},vtype="car",blipid=72,blipcolor=7},
    _shop = {
    -- You can make different shops with different modifications for each garage of gtype shop
      [0] = {"Spoilers",500,""},
      [1] = {"Front Bumper",500,""},
      [2] = {"Rear Bumper",500,""}, 
      [3] = {"Side Skirt",500,""},  
      [4] = {"Exhaust",500,""},     
      [5] = {"Frame",500,""},       
      [6] = {"Grille",500,""},      
      [7] = {"Hood",500,""},        
      [8] = {"Fender",500,""},      
      [9] = {"Right Fender",500,""},
      [10] = {"Roof",500,""},        
      [11] = {"Engine",500,""},      
      [12] = {"Brakes",500,""},      
      [13] = {"Transmission",500,""},
      [14] = {"Horns",500,""},       
      [15] = {"Suspension",500,""},  
      [16] = {"Armor",500,""},      
      [18] = {"Turbo",500,""},
      [20] = {"Tire Smoke",500,""},
      [22] = {"Xenon Headlights",500,""},
      [23] = {"Wheels",500,"Press enter to change wheel type"},
      [24] = {"Back Wheels (Bike)",500,""}, 
      [25] = {"Plateholders",500,""},
      [27] = {"Trims",500,""},       
      [28] = {"Ornaments",500,""},   
      [29] = {"Dashboards",500,""},  
      [30] = {"Dials",500,""},       
      [31] = {"Door Speakers",500,""},
      [32] = {"Seats",500,""},       
      [33] = {"Steering Wheel",500,""},
      [34] = {"H Shift",500,""},     
      [35] = {"Plates",500,""},      
      [36] = {"Speakers",500,""},    
      [37] = {"Trunks",500,""},      
      [38] = {"Hydraulics",500,""},  
      [39] = {"Engine Block",500,""},
      [40] = {"Air Filter",500,""},  
      [41] = {"Struts",500,""},      
      [42] = {"Arch Covers",500,""}, 
      [43] = {"Arials",500,""},      
      [44] = {"Extra Trims",500,""}, 
      [45] = {"Tanks",500,""},       
      [46] = {"Windows",500,""},     
      [48] = {"Livery",500,""},      
    }
  },
}

-- position garages on the map {garage_type,x,y,z}
cfg.garages = {
  
  -- personal garages
  {"Particular",215.124,-791.377,30.646},
  {"Particular",-334.685,289.773,85.705},
  {"Particular",-55.272,-1838.71,26.442},
  {"Particular",126.434,6610.04,31.750},
  
  -- lscustoms
  {"LS Customs",-337.3863,-136.9247,39.0737},
  {"LS Customs",-1155.536,-2007.183,13.244},
  {"LS Customs",731.8163,-1088.822,22.233},
  {"LS Customs",1175.04,2640.216,37.82177},
  {"LS Customs",110.8406,6626.568,32.287},
  
  -- house garages
  {"Fazenda",1408.32495117188,1117.44665527344,114.737692260742},
  {"Apartamento de luxo",-751.5107421875,365.883117675781,87.9666687011719},
  {"Apartamento Simples 1",-635.4501953125,57.4368324279785,44.8587303161621},
  {"Apartamento Simples 2", -1448.18701171875,-514.856567382813,31.6881823348999},


  --------------------------------LOJA DE VENDAS-------------------------------------------

  {"Compactos",-27.485260009766,-1103.8284912109,26.422365188599},
  {"Coupe",-32.08634185791,-1105.4647216797,26.422353744507},
  {"Esportivos",-30.567770004272,-1109.6118164063,26.422353744507},
  {"Classicos",-31.075899124146,-1113.0412597656,26.422353744507},
  {"Super Esportivos",-1448.18701171875,-514.856567382813,31.6881823348999},
  {"Carros de Alta Potencia",-53.659091949463,-1089.0299072266,26.422353744507},
  {"Off-Road",-56.736572265625,-1090.8947753906,26.422353744507},
  {"Utilitarios",-52.688137054443,-1100.5604248047,26.422353744507},
  {"Vans",-52.118286132813,-1102.5119628906,26.422353744507},
  {"Sedans",-44.473327636719,-1105.8760986328,26.422353744507},
  {"Motos",-42.447513580322,-1103.9445800781,26.422365188599,
  {"Populares",-43.648574829102,-1092.4317626953,26.422365188599},
  {"Coronel Polícia Militar",453.24819946289,-1019.3926391602,28.396326065063},
  {"Coronel Polícia Militar",1871.9401855469,3688.9060058594,33.641696929932},
  {"Coronel Polícia Militar",827.94219970703,-1258.1525878906,26.283124923706},
  {"Coronel Polícia Militar",901.92004394531,-8.369234085083,78.764854431152},
  {"Coronel Polícia Militar",374.07794189453,-1620.5498046875,29.291946411133},
  {"Coronel ROTA",453.24819946289,-1019.3926391602,28.396326065063},
  {"Coronel ROTA",1871.9401855469,3688.9060058594,33.641696929932},
  {"Coronel ROTA",827.94219970703,-1258.1525878906,26.283124923706},
  {"Coronel ROTA",901.92004394531,-8.369234085083,78.764854431152},
  {"Coronel ROTA",374.07794189453,-1620.5498046875,29.291946411133},
  {"Polícia Militar",453.24819946289,-1019.3926391602,28.396326065063},
  {"Polícia Militar",229.58934020996,-367.59896850586,44.163722991943},
  {"Polícia Militar",1871.9401855469,3688.9060058594,33.641696929932},
  {"Polícia Militar",827.94219970703,-1258.1525878906,26.283124923706},
  {"Polícia Militar",374.07794189453,-1620.5498046875,29.291946411133},
  {"Polícia Rodoviária Federal",-461.98156738281,6019.5883789063,30.6907081604},
  {"Polícia Rodoviária Federal",-475.07077026367,5988.4794921875,31.33670425415},
  {"Polícia Civil",453.24819946289,-1019.3926391602,28.396326065063},
  {"Polícia Civil",229.58934020996,-367.59896850586,44.163722991943},
  {"Polícia Civil",1871.9401855469,3688.9060058594,33.641696929932},
  {"Polícia Civil",827.94219970703,-1258.1525878906,26.283124923706},
  {"Polícia Civil",374.07794189453,-1620.5498046875,29.291946411133},
  {"ROCAM Polícia Militar",453.24819946289,-1019.3926391602,28.396326065063},
  {"ROCAM Polícia Militar",229.58934020996,-367.59896850586,44.163722991943},
  {"ROCAM Polícia Militar",1871.9401855469,3688.9060058594,33.641696929932},
  {"ROCAM Polícia Militar",827.94219970703,-1258.1525878906,26.283124923706},
  {"ROCAM Polícia Militar",374.07794189453,-1620.5498046875,29.291946411133},
  {"ROTA",453.24819946289,-1019.3926391602,28.396326065063},
  {"ROTA",229.58934020996,-367.59896850586,44.163722991943},
  {"ROTA",1871.9401855469,3688.9060058594,33.641696929932},
  {"ROTA",827.94219970703,-1258.1525878906,26.283124923706},
  {"ROTA",901.92004394531,-8.369234085083,78.764854431152},
  {"Helicóptero ROTA",885.0830078125,-40.144481658936,79.643371582031},
  {"Força Tática Polícia Militar",453.24819946289,-1019.3926391602,28.396326065063},
  {"Força Tática Polícia Militar",229.58934020996,-367.59896850586,44.163722991943},
  {"Força Tática Polícia Militar",1871.9401855469,3688.9060058594,33.641696929932},
  {"Força Tática Polícia Militar",827.94219970703,-1258.1525878906,26.283124923706},
  {"Força Tática Polícia Militar",374.07794189453,-1620.5498046875,29.291946411133},
  {"Águia Polícia Militar",450.23489379883,-981.12622070313,43.691707611084},
  {"Águia Polícia Militar",362.91030883789,-1598.5764160156,36.948837280273},
  {"Águia Polícia Militar",-1095.6641845703,-834.990234375,37.675407409668},
  {"Pelicano Polícia Civil",450.23489379883,-981.12622070313,43.691707611084},
  {"Pelicano Polícia Civil",362.91030883789,-1598.5764160156,36.948837280273},
  {"Pelicano Polícia Civil",-1095.6641845703,-834.990234375,37.675407409668},
  {"Particular",419.85659790039,-1029.5887451172,29.119018554688},
  {"Particular",760.22216796875,-312.05590820313,59.88151550293},
  {"Particular",-626.11346435547,56.508075714111,43.727066040039},
  {"Particular",1387.8536376953,-578.04730224609,74.338775634766},
  {"Particular",288.9680480957,-338.53717041016,44.919883728027},
  {"Particular",-78.805084228516,-787.44476318359,38.250644683838},
  {"Particular",-1444.3962402344,-522.58544921875,31.581819534302},
  {"Particular",-796.17144775391,323.05972290039,85.700492858887},
  {"Particular",-301.05477905273,-986.08465576172,31.080602645874},
  {"Particular",-59.516288757324,-1115.2729492188,26.435451507568},
  {"Particular",-557.84838867188,302.30114746094,83.20947265625},
  {"Particular",946.95208740234,-2189.4978027344,30.551580429077},
  {"Particular",2540.1843261719,-282.05697631836,92.8955078125},
  {"Particular",1993.8846435547,3035.0297851563,47.027317047119},
  {"Particular",1011.9776000977,-2302.517578125,30.50955772399},
  {"Particular",391.10906982422,-1618.5794677734,29.291948318481},
  {"Particular",200.70329284668,3173.0036621094,43.404472351074},
  {"Particular",-953.49176025391,-3029.4934082031,13.945063591003},
  {"Particular",-1201.2845458984,-1483.9542236328,4.3796710968018},
  {"Particular",147.95959472656,-1293.9560546875,29.309484481812},
  {"Particular",1200.7490234375,-1494.6657714844,34.692523956299},

  --Casas Vips
  {"Particular",1410.3022460938,1117.7930908203,114.83800506592},
  {"Particular",-189.15110778809,501.62377929688,134.48828125},
  {"Particular",353.81744384766,437.37255859375,146.6767578125},
  {"Particular",391.20950317383,430.60192871094,143.57209777832},
  {"Particular",-683.9697265625,602.63519287109,143.52864074707},
  {"Particular",-1271.8951416016,452.59371948242,95.021255493164},
  {"Particular",954.98699951172,-127.3816986084,74.370735168457},
  {"Particular",713.33404541016,-909.83569335938,23.805648803711},
  {"Particular",-821.18774414063,184.18444824219,72.052436828613},
  {"Particular",3623.9770507813,5001.9423828125,11.908643722534},
  {"Particular",2822.1674804688,-685.99768066406,1.1885861158371}, 
  {"Particular",-126.13877868652,999.20379638672,235.73585510254}
  -- planes and boats
  --{"Boats",-849.5, -1368.64, 1.6},
  --{"Civilian Planes",1640, 3236, 40.4},
  --{"Military Planes",-1348, -2230, 13.9},
  --{"Civilian Helicopters",1750, 3260, 41.37},
  --{"Military Helicopters",-1233, -2269, 13.9},
  --{"Civilian Planes",-1277.59094238281,-3391.39233398438,13.9401473999023},
  --{"Military Planes",1066.958984375,3078.87353515625,41.0369453430176},
  --{"Civilian Helicopters",-745.37255859375,-1468.43969726563,5.00051975250244},
  --{"Military Helicopters",1403.12109375,3002.36987304688,40.5485992431641},
  --{"planes",2123, 4805, 41.19},
  --{"helicopters",-745, -1468, 5},
  --{"Barcos",1538, 3902, 30.35}
  }
}

return cfg

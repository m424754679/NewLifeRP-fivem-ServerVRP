
local cfg = {}

cfg.firstjob = "Desempregado" -- set this to your first job, for example "citizen", or false to disable
cfg.showjob = true -- show job automatically with cfg.job css

-- job text display css
cfg.job = [[
.div_job{
  position: absolute;
  top: 155px;
  right: 20px;
  font-size: 20px;
  font-weight: bold;
  color: white;
  text-shadow: 3px 3px 2px rgba(0, 0, 0, 0.80);
}
]]

-- list of ["group"] => {text = "", css = [[ .div_group{ } ]]} for extra specific css
cfg.group = {
  ["[Recruta] - Polícia Militar"] {
    text = "",
  css = [[
    .div_recrutapm  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Soldado] - Polícia Militar"] {
    text = "",
  css = [[
    .div_sdpm  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Cabo] - Polícia Militar"] {
    text = "",
  css = [[
    .div_cabopm  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[3° Sargento] - Polícia Militar"] {
    text = "",
  css = [[
    .div_3sargpm  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[2° Sargento] - Polícia Militar"] {
    text = "",
  css = [[
    .div_2sargpm  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[1° Sargento] - Polícia Militar"] {
    text = "",
  css = [[
    .div_1sargpm  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Sub. Tenente] - Polícia Militar"] {
    text = "",
  css = [[
    .div_subtentpm  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[2° Tenente] - Polícia Militar"] {
    text = "",
  css = [[
    .div_2tentpm  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[1° Tenente] - Polícia Militar"] {
    text = "",
  css = [[
    .div_1tentpm  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Capitão] - Polícia Militar"] {
    text = "",
  css = [[
    .div_capitaopm  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Major] - Polícia Militar"] {
    text = "",
  css = [[
    .div_majorpm  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Recruta] - Força Tática"] {
    text = "",
  css = [[
    .div_recrutaft  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Soldado] - Força Tática"] {
    text = "",
  css = [[
    .div_sdft  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Cabo] - Força Tática"] {
    text = "",
  css = [[
    .div_caboft  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[3° Sargento] - Força Tática"] {
    text = "",
  css = [[
    .div_3sargft  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[2° Sargento] - Força Tática"] {
    text = "",
  css = [[
    .div_2sargft  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[1° Sargento] - Força Tática"] {
    text = "",
  css = [[
    .div_1sargft  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Sub. Tenente] - Força Tática"] {
    text = "",
  css = [[
    .div_subtentft  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[2° Tenente] - Força Tática"] {
    text = "",
  css = [[
    .div_2tentft  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[1° Tenente] - Força Tática"] {
    text = "",
  css = [[
    .div_1tentft  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Capitão] - Força Tática"] {
    text = "",
  css = [[
    .div_capitaoft  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Major] - Força Tática"] {
    text = "",
  css = [[
    .div_majorft  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Recruta] - ÁGUIA"] {
    text = "",
  css = [[
    .div_recrutaaguia  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Soldado] - ÁGUIA"] {
    text = "",
  css = [[
    .div_sdaguia  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Cabo] - ÁGUIA"] {
    text = "",
  css = [[
    .div_caboaguia #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[3° Sargento] - ÁGUIA"] {
    text = "",
  css = [[
    .div_3sargaguia  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[2° Sargento] - ÁGUIA"] {
    text = "",
  css = [[
    .div_2sargaguia  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[1° Sargento] - ÁGUIA"] {
    text = "",
  css = [[
    .div_1sargaguia #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Sub. Tenente] - ÁGUIA"] {
    text = "",
  css = [[
    .div_subtentaguia  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[2° Tenente] - ÁGUIA"] {
    text = "",
  css = [[
    .div_2tentaguia #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[1° Tenente] - ÁGUIA"] {
    text = "",
  css = [[
    .div_1tentaguia  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Capitão] - ÁGUIA"] {
    text = "",
  css = [[
    .div_capitaoaguia  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Major] - ÁGUIA"] {
    text = "",
  css = [[
    .div_majoraguia  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Recruta] - ROCAM"] {
    text = "",
  css = [[
    .div_recrutarocam  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Soldado] - ROCAM"] {
    text = "",
  css = [[
    .div_soladorocam  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Cabo] - ROCAM"] {
    text = "",
  css = [[
    .div_caborocam  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[3° Sargento] - ROCAM"] {
    text = "",
  css = [[
    .div_3sargrocam  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[2° Sargento] - ROCAM"] {
    text = "",
  css = [[
    .div_2sargrocam  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[1° Sargento] - ROCAM"] {
    text = "",
  css = [[
    .div_1sargrocam  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Sub. Tenente] - ROCAM"] {
    text = "",
  css = [[
    .div_subtentrocam  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[2° Tenente] - ROCAM"] {
    text = "",
  css = [[
    .div_2tentrocam  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[1° Tenente] - ROCAM"] {
    text = "",
  css = [[
    .div_1tentrocam  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Capitão] - ROCAM"] {
    text = "",
  css = [[
    .div_capitarocam  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Major] - ROCAM"] {
    text = "",
  css = [[
    .div_majorrocam  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Recruta] - ROTA"] {
    text = "",
  css = [[
    .div_recrutarota  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Soldado] - ROTA"] {
    text = "",
  css = [[
    .div_sdrota  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Cabo] - ROTA"] {
    text = "",
  css = [[
    .div_caborota  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[3° Sargento] - ROTA"] {
    text = "",
  css = [[
    .div_3sargrota  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[2° Sargento] - ROTA"] {
    text = "",
  css = [[
    .div_2sargrota  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[1° Sargento] - ROTA"] {
    text = "",
  css = [[
    .div_job  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Sub. Tenente] - ROTA"] {
    text = "",
  css = [[
    .div_subtentrota  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[2° Tenente] - ROTA"] {
    text = "",
  css = [[
    .div_2tentrota  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[1° Tenente] - ROTA"] {
    text = "",
  css = [[
    .div_1tentrota  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Capitão] - ROTA"] {
    text = "",
  css = [[
    .div_capitarota  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Major] - ROTA"] {
    text = "",
  css = [[
    .div_majorrota  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Tenente.Coronel] - ROTA"] {
    text = "",
  css = [[
    .div_tcoronelrota  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Coronel] - ROTA"] {
    text = "",
  css = [[
    .div_coronelrota  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Tenente.Coronel]"] {
    text = "",
  css = [[
    .div_tcoronelpm  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["[Coronel]"] {
    text = "",
  css = [[
    .div_coronelpm  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["Paramédico"] {
    text = "",
  css = [[
    .div_medico  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/SAMU.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["Mecânico"] {
    text = "",
  css = [[
    .div_mecanico  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Mecanico.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["Taxista"] {
    text = "",
  css = [[
    .div_taxi  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Taxi.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["Uber"] {
    text = "",
  css = [[
    .div_uber  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Uber.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["Entregador"] {
    text = "",
  css = [[
    .div_entregador  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Caixa.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["Ladrão de Carros"] {
    text = "",
  css = [[
    .div_ladcarro  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/LadraoDeCarros.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["Traficante de Maconha"] {
    text = "",
  css = [[
    .div_maconha  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Maconha.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["Traficante de Metanfetamina"] {
    text = "",
  css = [[
    .div_meta  #icon{
      position: absolute;
      content: url();
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["Traficante de Cocaina"] {
    text = "",
  css = [[
    .div_cocaina  #icon{
      position: absolute;
      content: url();
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["Traficante de Heroina"] {
    text = "",
  css = [[
    .div_heroina  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Heroina.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["Traficante de Armas"] {
    text = "",
  css = [[
    .div_armas  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Armas.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["Assassino Profissional"] {
    text = "",
  css = [[
    .div_hitman  #icon{
      position: absolute;
      content: url(http://www.freeiconspng.com/uploads/gold-police-badge-icon-9.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["hacker"] {
    text = "",
  css = [[
    .div_hack  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Hacker.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["mugger"] {
    text = "",
  css = [[
    .div_mugger  #icon{
      position: absolute;
      content: url(http://www.freeiconspng.com/uploads/gold-police-badge-icon-9.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["Jornalista"] {
    text = "",
  css = [[
    .div_jornalista  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Jornal.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["Editor Chefe"] {
    text = "",
  css = [[
    .div_editochefe  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Jornalista.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["Advogado"] {
    text = "",
  css = [[
    .div_advagado  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Advogado.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["Transportador de Valores"] {
    text = "",
  css = [[
    .div_transvalor  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Valores.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }
  ]],
  },
  ["Desempregado"] {
    text = "",
  css = [[
    .div_desempregado  #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Desempregado.png);
      height: 50px;
      width: 50px;
      top: 180px;
      right: 20px;
    }    
  ]], -- this is an example, add more under it using the same model, leave the } at the end.
}, -- commas are impotant and so are { }
  ["admin"] = {
    text = "Admin", -- groups need text and icon css
	css = [[
	  .div_admin{
		position: absolute;
		top: 230px;
		right: 20px;
		font-size: 20px;
		font-family: Pricedown !important;
		color: white;
		text-shadow: black 0 0 10px;
	  }
      .div_admin #icon{
        position: absolute;
        content: url(http://www.freeiconspng.com/uploads/gold-police-badge-icon-9.png);
        height: 30px;
        width: 30px;
		    top: 15px;
        right: 20px;
      }
    ]],-- this is an example, add more under it using the same model, leave the } at the end.
  },
  -- add more here
}
return cfg


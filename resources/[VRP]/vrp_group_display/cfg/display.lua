
local cfg = {}

cfg.firstjob = "Desempregado" -- set this to your first job, for example "citizen", or false to disable
cfg.showjob = true -- show job automatically with cfg.job css

-- job text display css
cfg.job = [[
.div_job{
  position: absolute;
  top: 155px;
  right: 20px;
  font-size: 20px;
  font-weight: bold;
  color: white;
  text-shadow: 3px 3px 2px rgba(0, 0, 0, 0.80);
}
]]

-- list of ["group"] => {text = "", css = [[ .div_group{ } ]]} for extra specific css
cfg.group = {
  ["[Recruta] - Polícia Militar"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Soldado] - Polícia Militar"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Cabo] - Polícia Militar"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[3º Sargento] - Polícia Militar"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[2º Sargento] - Polícia Militar"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[1º Sargento] - Polícia Militar"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Sub-Tenente] - Polícia Militar"] = {
    text = "",
  css = [[
    .div_jobpm #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[2º Tenente] - Polícia Militar"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[1º Tenente] - Polícia Militar"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Capitão] - Polícia Militar"] = {
    text = "",
  css = [[
    .div_jobpm #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Major] - Polícia Militar"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Tenente-Coronel] - Polícia Militar"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Coronel] - Polícia Militar"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Recruta] - Força Tática"] = {
    text = "",
  css = [[
    .div_jobft #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Soldado] - Força Tática"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Cabo] - Força Tática"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[3º Sargento] - Força Tática"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[2º Sargento] - Força Tática"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[1º Sargento] - Força Tática"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Sub-Tenente] - Força Tática"] = {
    text = "",
  css = [[
    .div_jobft #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[2º Tenente] - Força Tática"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[1º Tenente] - Força Tática"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Capitão] - Força Tática"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Major] - Força Tática"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Tenente-Coronel] - Força Tática"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Coronel] - Força Tática"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Recruta] - GRPAe"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Soldado] - GRPAe"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Cabo] - GRPAe"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[3º Sargento] - GRPAe"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[2º Sargento] - GRPAe"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[1º Sargento] - GRPAe"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Sub-Tenente] - GRPAe"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[2º Tenente] - GRPAe"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[1º Tenente] - GRPAe"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Capitão] - GRPAe"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Major] - GRPAe"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Tenente-Coronel] - GRPAe"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Coronel] - GRPAe"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Aguia.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Recruta] - ROCAM"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Soldado] - ROCAM"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Cabo] - ROCAM"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[3º Sargento] - ROCAM"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[2º Sargento] - ROCAM"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[1º Sargento] - ROCAM"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Sub-Tenente] - ROCAM"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[2º Tenente] - ROCAM"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[1º Tenente] - ROCAM"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Capitão] - ROCAM"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Major] - ROCAM"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Tenente-Coronel] - ROCAM"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Coronel] - ROCAM"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Rocam.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Recruta] - ROTA"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Soldado] - ROTA"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Cabo] - ROTA"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[3º Sargento] - ROTA"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[2º Sargento] - ROTA"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[1º Sargento] - ROTA"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Sub-Tenente] - ROTA"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[2º Tenente] - ROTA"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[1º Tenente] - ROTA"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Capitão] - ROTA"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Major] - ROTA"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Tenente-Coronel] - ROTA"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Coronel] - ROTA"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/ROTA.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Tenente-Coronel]"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Coronel]"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/PoliciaMilitar.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Delegado] - Polícia Civil"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://www.ouvidoria-policia.sp.gov.br/img/brasao_policia_civil_sp.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Perito] - Polícia Civil"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://www.ouvidoria-policia.sp.gov.br/img/brasao_policia_civil_sp.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Investigador] - Polícia Civil"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://www.ouvidoria-policia.sp.gov.br/img/brasao_policia_civil_sp.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Escrivão] - Polícia Civil"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://www.ouvidoria-policia.sp.gov.br/img/brasao_policia_civil_sp.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Inspetor] - Polícia Civil"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://www.ouvidoria-policia.sp.gov.br/img/brasao_policia_civil_sp.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Agente Especial] - Polícia Civil"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://www.ouvidoria-policia.sp.gov.br/img/brasao_policia_civil_sp.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Agente] - Polícia Civil"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://www.ouvidoria-policia.sp.gov.br/img/brasao_policia_civil_sp.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["[Soldado] - Polícia Rodoviária Federal"] = {
    text = "",
    css = [[
      .div_job #icon{
        position: absolute;
        content: url(https://www.radionavegantes.com.br/wp-content/uploads/2017/05/PRF.png);
        height: 50px;
        width: 50px;
        top: 15px;
        right: 20px;
      }
    ]],
  },
  ["[Agente] - Polícia Rodoviária Federal"] = {
    text = "",
    css = [[
      .div_job #icon{
        position: absolute;
        content: url(https://www.radionavegantes.com.br/wp-content/uploads/2017/05/PRF.png);
        height: 50px;
        width: 50px;
        top: 15px;
        right: 20px;
      }
    ]],
  },
  ["[Agente Operacional] - Polícia Rodoviária Federal"] = {
    text = "",
    css = [[
      .div_job #icon{
        position: absolute;
        content: url(https://www.radionavegantes.com.br/wp-content/uploads/2017/05/PRF.png);
        height: 50px;
        width: 50px;
        top: 15px;
        right: 20px;
      }
    ]],
  },
  ["[Inspetor] - Polícia Rodoviária Federal"] = {
    text = "",
    css = [[
      .div_job #icon{
        position: absolute;
        content: url(https://www.radionavegantes.com.br/wp-content/uploads/2017/05/PRF.png);
        height: 50px;
        width: 50px;
        top: 15px;
        right: 20px;
      }
    ]],
  },
  ["[Agente Especial] - Polícia Rodoviária Federal"] = {
    text = "",
    css = [[
      .div_job #icon{
        position: absolute;
        content: url(https://www.radionavegantes.com.br/wp-content/uploads/2017/05/PRF.png);
        height: 50px;
        width: 50px;
        top: 15px;
        right: 20px;
      }
    ]],
  },
  ["[Sargento] - Polícia Rodoviária Federal"] = {
    text = "",
    css = [[
      .div_job #icon{
        position: absolute;
        content: url(https://www.radionavegantes.com.br/wp-content/uploads/2017/05/PRF.png);
        height: 50px;
        width: 50px;
        top: 15px;
        right: 20px;
      }
    ]],
  },
  ["[Aspirante] - Polícia Rodoviária Federal"] = {
    text = "",
    css = [[
      .div_job #icon{
        position: absolute;
        content: url(https://www.radionavegantes.com.br/wp-content/uploads/2017/05/PRF.png);
        height: 50px;
        width: 50px;
        top: 15px;
        right: 20px;
      }
    ]],
  },
  ["[Capitão] - Polícia Rodoviária Federal"] = {
    text = "",
    css = [[
      .div_job #icon{
        position: absolute;
        content: url(https://www.radionavegantes.com.br/wp-content/uploads/2017/05/PRF.png);
        height: 50px;
        width: 50px;
        top: 15px;
        right: 20px;
      }
    ]],
  },
  ["[Major] - Polícia Rodoviária Federal"] = {
    text = "",
    css = [[
      .div_job #icon{
        position: absolute;
        content: url(https://www.radionavegantes.com.br/wp-content/uploads/2017/05/PRF.png);
        height: 50px;
        width: 50px;
        top: 15px;
        right: 20px;
      }
    ]],
  },
  ["[Delegado] - Polícia Rodoviária Federal"] = {
    text = "",
    css = [[
      .div_job #icon{
        position: absolute;
        content: url(https://www.radionavegantes.com.br/wp-content/uploads/2017/05/PRF.png);
        height: 50px;
        width: 50px;
        top: 15px;
        right: 20px;
      }
    ]],
  },
  ["Paramédico"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/SAMU.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["Mecânico"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Mecanico.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["Taxista"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Taxi.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["Uber"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Uber.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["Entregador"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Caixa.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["Ladrão de Carros"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/LadraoDeCarros.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["Traficante de Maconha"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Maconha.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["Traficante de Metanfetamina"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url();
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["Traficante de Cocaina"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url();
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["Traficante de Heroina"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Heroina.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["Traficante de Armas"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Armas.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["Assassino Profissional"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://www.freeiconspng.com/uploads/gold-police-badge-icon-9.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["hacker"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Hacker.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["mugger"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://www.freeiconspng.com/uploads/gold-police-badge-icon-9.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["Jornalista"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Jornal.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["Editor Chefe"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Jornalista.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["Advogado"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Advogado.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["Transportador de Valores"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Valores.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }
  ]],
  },
  ["Desempregado"] = {
    text = "",
  css = [[
    .div_job #icon{
      position: absolute;
      content: url(http://brasilnewliferp.com/img/Desempregado.png);
      height: 50px;
      width: 50px;
      top: 15px;
      right: 20px;
    }    
  ]], -- this is an example, add more under it using the same model, leave the } at the end.
}, -- commas are impotant and so are { }
  ["admin"] = {
    text = "Admin", -- groups need text and icon css
  css = [[
    .div_admin{
    position: absolute;
    top: 230px;
    right: 20px;
    font-size: 20px;
    font-family: Pricedown !important;
    color: white;
    text-shadow: black 0 0 10px;
    }
      .div_admin #icon{
        position: absolute;
        content: url(http://www.freeiconspng.com/uploads/gold-police-badge-icon-9.png);
        height: 30px;
        width: 30px;
        top: 15px;
        right: 20px;
      }
    ]],-- this is an example, add more under it using the same model, leave the } at the end.
  },
  -- add more here
}
return cfg


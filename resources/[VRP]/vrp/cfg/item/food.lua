-- define some basic inventory items

local items = {}

local function play_eat(player)
  local seq = {
    {"mp_player_inteat@burger", "mp_player_int_eat_burger_enter",1},
    {"mp_player_inteat@burger", "mp_player_int_eat_burger",1},
    {"mp_player_inteat@burger", "mp_player_int_eat_burger_fp",1},
    {"mp_player_inteat@burger", "mp_player_int_eat_exit_burger",1}
  }

  vRPclient.playAnim(player,{true,seq,false})
end

local function play_drink(player)
  local seq = {
    {"mp_player_intdrink","intro_bottle",1},
    {"mp_player_intdrink","loop_bottle",1},
    {"mp_player_intdrink","outro_bottle",1}
  }

  vRPclient.playAnim(player,{true,seq,false})
end

-- gen food choices as genfunc
-- idname
-- ftype: eat or drink
-- vary_hunger
-- vary_thirst
local function gen(ftype, vary_hunger, vary_thirst)
  local fgen = function(args)
    local idname = args[1]
    local choices = {}
    local act = "Unknown"
    if ftype == "eat" then act = "Eat" elseif ftype == "drink" then act = "Drink" end
    local name = vRP.getItemName(idname)

    choices[act] = {function(player,choice)
      local user_id = vRP.getUserId(player)
      if user_id ~= nil then
        if vRP.tryGetInventoryItem(user_id,idname,1,false) then
          if vary_hunger ~= 0 then vRP.varyHunger(user_id,vary_hunger) end
          if vary_thirst ~= 0 then vRP.varyThirst(user_id,vary_thirst) end

          if ftype == "drink" then
            vRPclient.notify(player,{"~b~ Drinking "..name.."."})
            play_drink(player)
          elseif ftype == "eat" then
            vRPclient.notify(player,{"~o~ Eating "..name.."."})
            play_eat(player)
          end

          vRP.closeMenu(player)
        end
      end
    end}

    return choices
  end

  return fgen
end

-- DRINKS --

items["water"] = {"Garrafa d'água","Garrafa de água mineral sem gás - 500ml", gen("drink",0,-25),0.5}
items["milk"] = {"Leite em caixa","Caixa de leite semi-desnatado - 1.5l", gen("drink",0,-5),0.5}
items["coffee"] = {"Café expresso","Xícara de café expresso com creme feito na hora.", gen("drink",0,-10),0.2}
items["tea"] = {"Chá-verde gelado","Unidade de chá-verde gelado com xícara descartável.", gen("drink",0,-15),0.2}
items["icetea"] = {"Ice-Tea","Garrafa de Ice-Tea de limão - 1l", gen("drink",0,-20), 0.5}
items["orangejuice"] = {"Suco de laranja","Caixa pequena de suco de laranja - 750ml", gen("drink",0,-25),0.5}
items["gocagola"] = {"Coca-Cola","Lata de coca-cola zero/normal - 250ml", gen("drink",0,-35),0.3}
items["redgull"] = {"RedBull","Lata de energético RedBull - 300ml", gen("drink",0,-40),0.3}
items["lemonlimonad"] = {"Limonada","Copo de limonada feita na hora.", gen("drink",0,-45),0.3}
items["vodka"] = {"Vodka","Bebida alcóolica forte reforçada.", gen("drink",15,-65),0.5}

--FOOD

-- create Breed item
items["bread"] = {"Pão Frânces","Pacote com 3 unidades de pão frânces frescos.", gen("eat",-10,0),0.5}
items["donut"] = {"Donut","Doce típico Americano produzido na hora.", gen("eat",-15,0),0.2}
items["tacos"] = {"Tacos","Salgado típico da América do Norte com legumes frescos.", gen("eat",-20,0),0.2}
items["sandwich"] = {"Sanduíche","Sanduíche de queijo com presunto e salada.", gen("eat",-25,0),0.5}
items["kebab"] = {"Churrasco","Misto de carne, frango e linguiça em pedaços fisgados em palito.", gen("eat",-45,0),0.85}
items["maca"] = {"Maça","", gen("eat",-10,0),0.2}
items["banana"] = {"Banana","", gen("eat",-10,0),0.2}
items["pessegos"] = {"Pêssegos","", gen("eat",-10,0),0.2}
items["batatar"] = {"Batata Ruffles","", gen("eat",-10,0),0.2}
items["pdonut"] = {"Premium Donut","", gen("eat",-25,0),0.5}
items["bagre"] = {"Bagre","", gen("eat",10,15),0.3}
items["pirarucu"] = {"Pirarucu","", gen("eat",10,15),0.3}

return items

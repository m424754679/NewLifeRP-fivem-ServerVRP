
local cfg = {}

-- exp notes:
-- levels are defined by the amount of xp
-- with a step of 5: 5|15|30|50|75 (by default)
-- total exp for a specific level, exp = step*lvl*(lvl+1)/2
-- level for a specific exp amount, lvl = (sqrt(1+8*exp/step)-1)/2

-- define groups of aptitudes
--- _title: title of the group
--- map of aptitude => {title,init_exp,max_exp}
---- max_exp: -1 for infinite exp
cfg.gaptitudes = {
  ["physical"] = {
    _title = "Físico",
    ["strength"] = {"Força", 30, 550}, -- required, level 3 to 6 (by default, can carry 10kg per level)
    ["resistence"] = {"Resistência", 30, 550}
  },
  ["science"] = {
    _title = "Ciência",
    ["chemicals"] = {"Estudo sobre produtos químicos", 0, -1}, -- example
    ["math"] = {"Estudo de matemática", 0, -1}, -- example
    ["biology"] = {"Estudo de biologia", 0, -1}
  },
  ["laboratory"] = {
    _title = "Laboratório",
	  ["cocaine"] = {"Conhecimento no processamento de cocaína", 0, -1},
	  ["weed"] = {"Conhecimento no processamento de maconha", 0, -1},
	  ["lsd"] = {"Conhecimento no processamento de lsd", 0, -1}
  },
  ["technology"] = {
    _title = "",
	  ["logic"] = {"Estudo sobre a lógica da programação", 0, -1},
	  ["c++"] = {"Estudo de c++", 0, -1},
	  ["lua"] = {"Estudo de Lua", 0, -1},
    ["java"] = {"Estudo de Java", 0, -1},
    ["sql"] = {"Estudo sobre Banco de Dados", 0, -1},
	  ["hacker"] = {"Estudo Hacker", 0, -1}
  }
}

return cfg

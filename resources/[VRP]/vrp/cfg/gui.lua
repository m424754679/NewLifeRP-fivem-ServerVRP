
-- gui config file

local cfg = {}

-- additional css loaded to customize the gui display (see gui/design.css to know the available css elements)
-- it is not recommended to modify the vRP core files outside the cfg/ directory, create a new resource instead
-- you can load external images/fonts/etc using the NUI absolute path: nui://my_resource/myfont.ttf
-- example, changing the gui font (suppose a vrp_mod resource containing a custom font)
cfg.css =  [[
@font-face {
  font-family: "pcdown";
  src: url(nui://vrp/gui/fonts/pcdown.ttf) format("truetype");
}

@font-face {
  font-family: "bankgothic";
  src: url(nui://vrp/gui/fonts/bankgothic.ttf) format("truetype");
}

body{
  font-family: "Segoe UI";
  font-size: 0.9em;
}
]]
-- list of static menu types (map of name => {.title,.blipid,.blipcolor,.permissions (optional)})
-- static menus are menu with choices defined by vRP.addStaticMenuChoices(name, choices)
cfg.static_menu_types = { --[[
   ["police_weapons"] = {
      title = "Armas - Polícia Militar", 
      blipcolor = 0,
      permissions = {
      "armas.pm"
      }
   },
   ["pmft_weapons"] = {
      title = "Força Tática - Polícia Militar", 
      blipcolor = 0,
      permissions = {
      "armas.pmft"
    }
   },
   ["rota_weapons"] = {
      title = "ROTA - Polícia", 
      blipcolor = 0,
      permissions = {
      "armas.rota"
    }
   },
   ["pcivil_weapons"] = {
      title = "Agente - Polícia Civil", 
      blipcolor = 0,
      permissions = {
      "armas.pcbase"
    }
   },
   ["garra_weapons"] = {
      title = "GARRA - Polícia Civil", 
      blipcolor = 0,
      permissions = {
      "armas.pcgarra"
    }
   },
   ["prf_weapons"] = {
      title = "Armas - Polícia Rodoviária Federal", 
      blipcolor = 0,
      permissions = {
      "armas.prf"
    }
   },
   ["sniper_weapons"] = {
      title = "Armas - Atirador de elite", 
      blipcolor = 0,
      permissions = {
      "armas.sniper"
    }
   },
   ["bombeiros_kit"] = {
      title = "Kit - Bombeiros", 
      blipcolor = 0,
      permissions = {
      "kit.bombeiros"
    }
   },
   ["emergency_heal"] = {
      title = "Atendimento Médico", 
      blipcolor = 0,
    permissions = {
    "emergency_heal"
  }
  },
   ["emergency_medkit"] = {
      title = "Emergência", 
      blipcolor = 0,
    permissions = {
    "emergency.medkit"
  }
  },--]]
  ["missions"] = { -- example of a mission menu that can be filled by other resources
    title = "Missions",
    blipid = 205, 
    blipcolor = 5
  }
}

-- list of static menu points
cfg.static_menus = {  --[[
  {"police_weapons",461.31414794922,-981.15582275391,30.689588546753},
  {"police_weapons",397.48709106445,-1610.3175048828,19.167369842529},
  {"prf_weapons",-448.58901977539,6018.013671875,31.71639251709},
  {"pmft_weapons",461.31414794922,-981.15582275391,30.689588546753},
  {"pmft_weapons",397.48709106445,-1610.3175048828,19.167369842529},
  {"pmft_weapons",351.48681640625,-1611.1232910156,19.145702362061},
  {"rota_weapons",461.31414794922,-981.15582275391,30.689588546753},
  {"rota_weapons",397.48709106445,-1610.3175048828,19.167369842529},
  {"rota_weapons",351.48681640625,-1611.1232910156,19.145702362061},
  {"pcivil_weapons",461.31414794922,-981.15582275391,30.689588546753},
  {"pcivil_weapons",397.48709106445,-1610.3175048828,19.167369842529},
  {"pcivil_weapons",351.48681640625,-1611.1232910156,19.145702362061},
  {"garra_weapons",461.31414794922,-981.15582275391,30.689588546753},
  {"garra_weapons",397.48709106445,-1610.3175048828,19.167369842529},
  {"garra_weapons",351.48681640625,-1611.1232910156,19.145702362061},
  {"emergency_medkit",268.22784423828,-1364.8872070313,24.537782669067},
  {"bombeiros_kit",268.22784423828,-1364.8872070313,24.537782669067},
  {"emergency_heal",260.49597167969,-1358.4555664063,24.537788391113}--]]
}

return cfg

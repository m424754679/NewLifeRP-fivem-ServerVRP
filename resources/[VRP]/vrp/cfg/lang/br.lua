-- define all language properties

local lang = {
  common = {
    welcome = "Bem-vindo ao Brasil NewLife! Utilize a tecla ~r~K~w~ para acessar o menu de interação do jogador.",
    no_player_near = "~r~Nenhum jogador próximo.",
    invalid_value = "~r~Valor inválido.",
    invalid_name = "~r~Nome inválido.",
    not_found = "~r~Não encontrado.",
    request_refused = "~r~Solicitação recusada.",
    wearing_uniform = "~r~Tome cuidado, você está vestindo um uniforme.",
    not_allowed = "~r~Não permitido."
  },
  weapon = {
    pistol = "Pistola"
  },
  survival = {
    starving = "Você está morrendo de fome!",
    thirsty = "Você está morrendo de sede!"
  },
  money = {
    display = "<span class=\"symbol\">R$</span> {1}",
    given = "Foram enviados ~r~R${1}~w~.",
    received = "Foram recebidos ~g~R${1}~w~.",
    not_enough = "~r~Dinheiro insuficiente.",
    paid = "Foram pagos ~r~R${1}~w~.",
    give = {
      title = "Enviar dinheiro",
      description = "Envie dinheiro para o jogador mais próximo.",
      prompt = "Quantidade para enviar:"
    }
  },
  inventory = {
    title = "Inventário",
    description = "Abrir o iventário.",
    iteminfo = "({1})<br /><br />{2}<br /><em>{3} kg</em>",
    info_weight = "Peso no inventário: {1}/{2}kg",
    give = {
      title = "Enviar",
      description = "Envie items ao jogador mais próximo.",
      prompt = "Quantidade para enviar (máximo {1}):",
      given = "Foram enviados ~r~{1} ~s~{2}.",
      received = "Foram recebidos ~g~{1} ~s~{2}.",
    },
    trash = {
      title = "Jogar fora (destruir)",
      description = "Destrua certa quantia de items desse tipo.",
      prompt = "Quantidade para destruir (máximo {1}):",
      done = "Foram destruidos ~r~{1} ~s~{2}."
    },
    missing = "Falta(m) ~r~{2} {1}~w~.",
    full = "~r~Inventário cheio.",
    chest = {
      title = "Baú",
      already_opened = "~r~Este baú já foi aberto por outra pessoa.",
      full = "~r~Baú cheio.",
      take = {
        title = "Pegar",
        prompt = "Quantidade para pegar (máximo {1}):"
      },
      put = {
        title = "Colocar",
        prompt = "Quantidade para colocar (máximo {1}):"
      }
    }
  },
  atm = {
    title = "Caixa eletrônico",
    info = {
      title = "Informações",
      bank = "saldo da conta: {1} $"
    },
    deposit = {
      title = "Depositar",
      description = "Depoistar dinheiro na conta.",
      prompt = "Quantidade para depoistar:",
      deposited = "Foram depositados ~r~R${1}."
    },
    withdraw = {
      title = "Sacar",
      description = "Sacar dinheiro da conta.",
      prompt = "Quantidade para sacar:",
      withdrawn = "Foram sacados ~g~R${1}.",
      not_enough = "~r~Dinheiro em conta insuficiente."
    }
  },
  business = {
    title = "Centro comercial",
    directory = {
      title = "Escritório",
      description = "Escritório executivo.",
      dprev = "> Voltar",
      dnext = "> Avançar",
      info = "<em>Capital: </em>R${1}<br /><em>Proprietário: </em>{2} {3}<br /><em>Registro n°: </em>{4}<br /><em>Telefone: </em>{5}"
    },
    info = {
      title = "Informações comerciais",
      info = "<em>Nome: </em>{1}<br /><em>Capital: </em>R${2}<br /><em>Transferir capital: </em>R${3}<br /><br/>A transferência de capital é a quantia de dinheiro transferida para um período econômico comercial, o máximo é o capital da empresa."
    },
    addcapital = {
      title = "Adicionar capital",
      description = "Adicione capital para sua empresa.",
      prompt = "Quantidade para adicionar:",
      added = "Foram adicionados ~r~R${1} ~s~de capital para sua empresa."
    },
    launder = {
      title = "Lavagem de dinheiro",
      description = "Utilize o nome de sua empresa para lavar dinheiro.",
      prompt = "Quantidade para lavar (máximo R${1}): ",
      laundered = "Foram limpos ~g~R${1}~s~.",
      not_enough = "~r~Dinheiro sujo insuficiente."
    },
    open = {
      title = "Abrir negócios",
      description = "Abra sua empresa (capital mínima de R${1}).",
      prompt_name = "Nome da empresa (impossível de alterar, máximo de {1} caractéres):",
      prompt_capital = "Capital inicial (mínimo R${1}):",
      created = "~g~Negócios abertos."
      
    }
  },
  cityhall = {
    title = "Prefeitura",
    identity = {
      title = "Nova identidade",
      description = "Emitir nova identidade (custo: R${1})",
      prompt_firstname = "Primeiro nome:",
      prompt_name = "Sobrenome:",
      prompt_age = "Idade:",
    },
    menu = {
      title = "Identidade",
      info = "<em>Nome: </em>{1}<br /><em>Sobrenome: </em>{2}<br /><em>Idade: </em>{3}<br /><em>RG n°: </em>{4}<br /><em>Telefone: </em>{5}<br /><em>Endereço: </em>{6}, unidade {7}"
    }
  },
  police = {
    title = "Ações do jogador",
    wanted = "Nível de procurado: {1}",
    not_handcuffed = "~r~Meliante não algemado",
    cloakroom = {
      title = "Vestiário",
      uniform = {
        title = "Uniforme",
        description = "Colocar uniforme."
      }
    },
    pc = {
      title = "SNR",
      searchreg = {
        title = "Pesquisar RG",
        description = "Pesquisar dados pelo RG.",
        prompt = "Número de registro:"
      },
      closebusiness = {
        title = "Fechar negócios",
        description = "Feche os negócios do jogador mais próximo.",
        request = "Tem certeza que deseja fechar a empresa {3} de {1} {2}?",
        closed = "~g~Negócios fechados."
      },
      trackveh = {
        title = "Rastrear veículos",
        description = "Restrear veíuclos pela placa registrada.",
        prompt_reg = "Digite a placa do veículo:",
        prompt_note = "Digite a nota de rastreamento:",
        tracking = "~b~Rastreamento iniciado.",
        track_failed = "~b~O rastreamento de {1}~s~ ({2}) ~n~~r~ falhou.",
        tracked = "Veículo rastreado: {1} ({2})"
      },
      records = {
        show = {
          title = "Verificar ficha criminal",
          description = "Procurar registros policiais pelo número de registro."
        },
        delete = {
          title = "Limpar ficha criminal",
          description = "Limpar registros policiais pelo número de registros.",
          deleted = "~b~Ficha criminal limpa."
        }
      }
    },
    menu = {
      handcuff = {
        title = "Algemas",
        description = "Algemar/desalgemar meliante."
      },
      putinveh = {
        title = "Colocar suspeito no veículo",
        description = "Forçar entrada do indivíduo algemado mais próximo no veículo."
      },
      getoutveh = {
        title = "Retirar suspeito do veículo",
        description = "Forçar saída do indivíduo algemado do veículo."
      },
      askid = {
        title = "Pedir RG",
        description = "Solicitar o ducumento de identificação do indivíduo mais próximo.",
        request = "Cidadão, poderia me fornecer sua identidade?",
        request_hide = "Fechar painel de informações?",
        asked = "Solicitando documento de identidade..."
      },
      check = {
        title = "Revistar jogador",
        description = "Verificar dinheiro, inventário e armas do jogador mais próximo.",
        request_hide = "Fechar relatório de checagem??",
        info = "<em>Dinheiro: </em>R${1}<br /><br /><em>Inventário: </em>{2}<br /><br /><em>Armas: </em>{3}",
        checked = "Você está sendo revistado."
      },
      seize = {
        seized = "Seized {2} ~r~{1}",
        weapons = {
          title = "Apreender/confiscar armas",
          description = "Confisque as armas do jogador mais próximo.",
          seized = "~b~Suas armas foram confiscadas."
        },
        items = {
          title = "Apreender itens ilegais",
          description = "Confisque os itens ilegais do jogador mais próximo.",
          seized = "~b~Seus itens ilegais foram apreendidos."
        }
      },
      jail = {
        title = "Prender/soltar",
        description = "Enviar ou retirar o jogador mais próximo da cadeia.",
        not_found = "~r~Nenhuma cadeia encontrada.",
        jailed = "~b~Jogador preso.",
        unjailed = "~b~Jogador solto.",
        notify_jailed = "~b~Você foi enviado à cadeia.",
        notify_unjailed = "~b~Você foi libertado da cadeia."
      },
      fine = {
        title = "Multar",
        description = "Multar jogador mais próximo.",
        fined = "~b~Aplicada multa de ~s~R${2} por ~b~{1}.",
        notify_fined = "~b~Você recebeu uma multa de ~s~R${2} por ~b~{1}.",
        record = "[Fine] {2} $ for {1}"
      },
      store_weapons = {
        title = "Guardar armas",
        description = "Enviar armas o inventário."
      }
    },
    identity = {
      info = "<em>Sobrenome: </em>{1}<br /><em>Primeiro nome: </em>{2}<br /><em>Idade: </em>{3}<br /><em>RG n°: </em>{4}<br /><em>Telefone: </em>{5}<br /><em>Empresa: </em>{6}<br /><em>Capital: </em>{7} $<br /><em>Endereço: </em>{8}, unidade {9}"
    }
  },
  emergency = {
    menu = {
      revive = {
        title = "Reaminar",
        description = "Reanime o jogador mais próximo.",
        not_in_coma = "~r~Jogador não está inconsciente.."
      }
    }
  },
  phone = {
    title = "Telefone",
    directory = {
      title = "Meus contatos",
      description = "Abrir lista de contatos armazenados.",
      add = {
        title = "> Adicionar",
        prompt_number = "Número a ser adicionado:",
        prompt_name = "Nome do contato:",
        added = "~g~Contato adicionado."
      },
      sendsms = {
        title = "Enviar SMS",
        prompt = "Mensagem a ser enviada (máximo {1} caractéres):",
        sent = "~g~SMS enviado para n°{1}.",
        not_sent = "~r~n°{1} indisponível."
      },
      sendpos = {
        title = "Enviar localização",
      },
      remove = {
        title = "Remover"
      }
    },
    sms = {
      title = "Histórico de mensagens",
      description = "Histórico de mensagens recebidas.",
      info = "<em>{1}</em><br /><br />{2}",
      notify = "SMS~b~ {1}:~s~ ~n~{2}"
    },
    smspos = {
      notify = "SMS-Localização ~b~ {1}"
    },
    service = {
      title = "Serviços",
      description = "Solicite um serviço de emergência.",
      prompt = "Se necessário, envie uma nota ao solicitado:",
      ask_call = "Chamada recebida para {1}. Deseja aceitá-la? <em>{2}</em>",
      taken = "~r~Esta chamada já foi recebida."
    },
    announce = {
      title = "Anunciar",
      description = "Publicar um anúncio visível para todos por alguns segundos.",
      item_desc = "R${1}<br /><br/>{2}",
      prompt = "Corpo do anúncio (10-1000 caractéres): "
    }
  },
  emotes = {
    title = "Animações",
    clear = {
      title = "> Parar",
      description = "Parar todas as animações do jogador."
    }
  },
  home = {
    buy = {
      title = "Comprar",
      description = "Adquira uma propriedade aqui. O preço é de ~b~R${1}~w~.",
      bought = "~g~Propriedade adquirida, parabéns!.",
      full = "~r~Este local está cheio.",
      have_home = "~r~Você já possui uma propriedade aqui."
    },
    sell = {
      title = "Vender",
      description = "Vender sua propriedade por ~r~R${1}",
      sold = "~g~Propriedade vendida.",
      no_home = "~r~Você não possui uma propriedade aqui."
    },
    intercom = {
      title = "Interfone",
      description = "Utilize o interfone para entrar em um apartamento.",
      prompt = "Número:",
      not_available = "~r~Unidade não disponível.",
      refused = "~r~Entrada recusada.",
      prompt_who = "Diga quem você é:",
      asked = "Aguardando resposta...",
      request = "Alguém quer adentrar sua propriedade: <em>{1}</em>"
    },
    slot = {
      leave = {
        title = "Sair"
      },
      ejectall = {
        title = "Retirar todos",
        description = "Forçar saída de todos os visitantes."
      }
    },
    wardrobe = {
      title = "Gaurda-Roupas",
      save = {
        title = "> Salvar",
        prompt = "Nome do traje:"
      }
    },
    gametable = {
      title = "Mesa de apostas",
      bet = {
        title = "Inicar jogo",
        description = "Iniciar aposta com jogador mais próximo, o vencedor será aleatoriamente escolhido.",
        prompt = "Valor da aposta:",
        request = "[APOSTA] Você gostaria de apostar ~g~R${1}~w~?",
        started = "~g~Aposta iniciada."
      }
    }
  },
  garage = {
    title = "Garagem ({1})",
    owned = {
      title = "Veículos possuidos",
      description = "Lista de veículos que você possui."
    },
    buy = {
      title = "Comprar",
      description = "Adquirir veículos.",
      info = "R${1}<br /><br />{2}"
    },
    sell = {
      title = "Vender",
      description = "Vender veículos."
    },
    rent = {
      title = "Alugar",
      description = "Alugar veículos por uma sessão (até você relogar)."
    },
    store = {
      title = "Guardar",
      description = "Colocar veículo atual na garagem."
    }
  },
  vehicle = {
    title = "Veículo",
    no_owned_near = "~r~Nenhum veículo seu próximo.",
    trunk = {
      title = "Porta-Malas",
      description = "Abrir o Porta-Malas do veículo."
    },
    detach_trailer = {
      title = "Desencaixar trailer",
      description = "Desencaixe o trailer atual."
    },
    detach_towtruck = {
      title = "Desencaixar reboque",
      description = "Desencaixe o reboque atual."
    },
    detach_cargobob = {
      title = "Desencaixar cargobob",
      description = "Desencaixe o veículo levado pelo cargobob."
    },
    lock = {
      title = "Travar/destravar",
      description = "Trave ou destrave as portas de seu veículo."
    },
    engine = {
      title = "Ligar/desligar motor",
      description = "Liga ou desliga a ignição do veículo."
    },
    asktrunk = {
      title = "Solicitar acessoa o Porta-Malas",
      asked = "~g~Aguardando resposta...",
      request = "Deseja fornecer acesso ao Porta-Malas?"
    },
    replace = {
      title = "Trocar veículo",
      description = "Substituir o veículo mais próximo a você."
    },
    repair = {
      title = "Reparar veículo",
      description = "Repare o veículo mais próximo a você."
    }
  },
  gunshop = {
    title = "Loja de armas ({1})",
    prompt_ammo = "Quantidade de munição para {1}:",
    info = "<em>Corpo: </em>R${1}<br /><em>Munição: </em>R${2}/u<br /><br />{3}"
  },
  market = {
    title = "Mercado ({1})",
    prompt = "Quantidade de {1} para comprar:",
    info = "R${1}<br /><br />{2}"
  },
  skinshop = {
    title = "Loja de roupas"
  },
  desbugskins = {
    title = "Vestuário"
  },
  cloakroom = {
    title = "Vestiário ({1})",
    undress = {
      title = "> Retirar uniforme"
    }
  },
  itemtr = {
    informer = {
      title = "Informante ilegal",
      description = "R${1}",
      bought = "~g~Posição enviada ao seu GPS."
    }
  },
  mission = {
    blip = "Missão ({1}) {2}/{3}",
    display = "<span class=\"name\">{1}</span> <span class=\"step\">{2}/{3}</span><br /><br />{4}",
    cancel = {
      title = "Cancelar missão"
    }
  },
  aptitude = {
    title = "Estatísticas",
    description = "Mostrar estatísticas.",
    lose_exp = "Aptidão ~b~{1}/{2} ~r~-{3} ~s~exp.",
    earn_exp = "Aptidão ~b~{1}/{2} ~g~+{3} ~s~exp.",
    level_down = "Aptidão ~b~{1}/{2} ~r~nível perdido ({3}).",
    level_up = "Aptidão ~b~{1}/{2} ~g~nível ganho ({3}).",
    display = {
      group = "{1}: ",
      aptitude = "{1} LVL {3} EXP {2}"
    }
  }
}

return lang

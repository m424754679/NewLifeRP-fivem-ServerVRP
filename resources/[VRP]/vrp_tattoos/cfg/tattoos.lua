
local cfg = {}

-- list of tattoos for sale
-- for the native name, see the tattoos folder, the native names of the tattoos is in the files with the native name of the it's shop
-- create groups like for the garage config
-- [display_name] = {native_tattoo_name,price,description}

-- _config: blipid, blipcolor, permissions (optional, only users with the permission will have access to the shop)
-- https://wiki.gtanet.work/index.php?title=Blips
-- https://wiki.fivem.net/wiki/Controls

cfg.tattoos = {
	["mpbeach_overlays"] = { -- native store name
		_config = {blipid=75,blipcolor=48,title="Tatuagens de Praias"}, -- you can add permissions like on other vRP features
		[">Clear Tattoos"] = {"CLEAR",10,""},
		["Head Tattoo 1"] = {"MP_Bea_M_Head_000",55,""},
		["Head Tattoo 2"] = {"MP_Bea_M_Head_001",55,""},
		["Head Tattoo 3"] = {"MP_Bea_M_Head_002",55,""},
		["Neck Tattoo 1"] = {"MP_Bea_F_Neck_000",55,""},
		["Neck Tattoo 2"] = {"MP_Bea_M_Neck_000",55,""},
		["Neck Tattoo 3"] = {"MP_Bea_M_Neck_001",55,""},
		["Back Tattoo 1"] = {"MP_Bea_F_Back_000",55,""},
		["Back Tattoo 2"] = {"MP_Bea_F_Back_001",55,""},
		["Back Tattoo 3"] = {"MP_Bea_F_Back_002",55,""},
		["Back Tattoo 4"] = {"MP_Bea_M_Back_000",55,""},
		["Torso Tattoo 1"] = {"MP_Bea_F_Chest_000",55,""},
		["Torso Tattoo 2"] = {"MP_Bea_F_Chest_001",55,""},
		["Torso Tattoo 3"] = {"MP_Bea_F_Chest_002",55,""},
		["Torso Tattoo 4"] = {"MP_Bea_M_Chest_000",55,""},
		["Torso Tattoo 5"] = {"MP_Bea_M_Chest_001",55,""},
		["Torso Tattoo 6"] = {"MP_Bea_F_Stom_000",55,""},
		["Torso Tattoo 7"] = {"MP_Bea_F_Stom_001",55,""},
		["Torso Tattoo 8"] = {"MP_Bea_F_Stom_002",55,""},
		["Torso Tattoo 9"] = {"MP_Bea_M_Stom_000",55,""}, 
		["Torso Tattoo 10"] = {"MP_Bea_M_Stom_001",55,""},
		["Torso Tattoo 11"] = {"MP_Bea_F_RSide_000",55,""},
		["Torso Tattoo 12"] = {"MP_Bea_F_Should_000",55,""},
		["Torso Tattoo 13"] = {"MP_Bea_F_Should_001",55,""},
		["Right Arm Tattoo 1"] = {"MP_Bea_F_RArm_001",55,""},
		["Right Arm Tattoo 2"] = {"MP_Bea_M_RArm_001",55,""},
		["Right Arm Tattoo 3"] = {"MP_Bea_M_RArm_000",55,""},
		["Left Arm Tattoo 1"] = {"MP_Bea_F_LArm_000",55,""},
		["Left Arm Tattoo 2"] = {"MP_Bea_F_LArm_001",55,""},
		["Left Arm Tattoo 3"] = {"MP_Bea_M_LArm_000",55,""}, 
		["Left Leg Tattoo"] = {"MP_Bea_M_Lleg_000",55,""},
		["Right Leg Tattoo"] = {"MP_Bea_F_RLeg_000",55,""}
	},
	["mpbusiness_overlays"] = {
		_config = {blipid=75,blipcolor=48,title="Tatuagens de Negócios"},
		[">Clear Tattoos"] = {"CLEAR",10,""},
		["Neck Tattoo 1"] = {"MP_Buis_M_Neck_000",55,""},
		["Neck Tattoo 2"] = {"MP_Buis_M_Neck_001",55,""},
		["Neck Tattoo 3"] = {"MP_Buis_M_Neck_002",55,""},
		["Neck Tattoo 4"] = {"MP_Buis_M_Neck_003",55,""},
		["Left Arm Tattoo 1"] = {"MP_Buis_M_LeftArm_000",55,""},
		["Left Arm Tattoo 2"] = {"MP_Buis_M_LeftArm_001",55,""},
		["Right Arm Tattoo 1"] = {"MP_Buis_M_RightArm_000",55,""},
		["Right Arm Tattoo 2"] = {"MP_Buis_M_RightArm_001",55,""},
		["Stomach Tattoo 1"] = {"MP_Buis_M_Stomach_000",55,""},
		["Chest Tattoo 1"] = {"MP_Buis_M_Chest_000",55,""},
		["Chest Tattoo 2"] = {"MP_Buis_M_Chest_001",55,""},
		["Back Tattoo 1"] = {"MP_Buis_M_Back_000",55,""},
		["Chest Tattoo 3"] = {"MP_Buis_F_Chest_000",55,""},
		["Chest Tattoo 4"] = {"MP_Buis_F_Chest_001",55,""},
		["Chest Tattoo 5"] = {"MP_Buis_F_Chest_002",55,""},
		["Stomach Tattoo 2"] = {"MP_Buis_F_Stom_000",55,""},
		["Stomach Tattoo 3"] = {"MP_Buis_F_Stom_001",55,""},
		["Stomach Tattoo 4"] = {"MP_Buis_F_Stom_002",55,""},
		["Back Tattoo 2"] = {"MP_Buis_F_Back_000",55,""},
		["Back Tattoo 3"] = {"MP_Buis_F_Back_001",55,""},
		["Neck Tattoo 5"] = {"MP_Buis_F_Neck_000",55,""},
		["Neck Tattoo 6"] = {"MP_Buis_F_Neck_001",55,""},
		["Right Arm Tattoo 3"] = {"MP_Buis_F_RArm_000",55,""},
		["Left Arm Tattoo 3"] = {"MP_Buis_F_LArm_000",55,""},
		["Left Leg Tattoo"] = {"MP_Buis_F_LLeg_000",55,""},
		["Right Leg Tattoo"] = {"MP_Buis_F_RLeg_000",55,""}

	},

	["mphipster_overlays"] = {
		_config = {blipid=75,blipcolor=48,title="Tatuagens Hipster"},
		[">Clear Tattoos"] = {"CLEAR",10,""},
		["HipsterTattoo 1"] = {"FM_Hip_M_Tat_000",55,""},
		["HipsterTattoo 2"] = {"FM_Hip_M_Tat_001",55,""},
		["HipsterTattoo 3"] = {"FM_Hip_M_Tat_002",55,""},
		["HipsterTattoo 4"] = {"FM_Hip_M_Tat_003",55,""},
		["HipsterTattoo 5"] = {"FM_Hip_M_Tat_004",55,""},
		["HipsterTattoo 6"] = {"FM_Hip_M_Tat_005",55,""},
		["HipsterTattoo 7"] = {"FM_Hip_M_Tat_006",55,""},
		["HipsterTattoo 8"] = {"FM_Hip_M_Tat_007",55,""},
		["HipsterTattoo 9"] = {"FM_Hip_M_Tat_008",55,""},
		["HipsterTattoo 10"] = {"FM_Hip_M_Tat_009",55,""},
		["HipsterTattoo 11"] = {"FM_Hip_M_Tat_010",55,""},
		["HipsterTattoo 12"] = {"FM_Hip_M_Tat_011",55,""},
		["HipsterTattoo 13"] = {"FM_Hip_M_Tat_012",55,""},
		["HipsterTattoo 14"] = {"FM_Hip_M_Tat_013",55,""},
		["HipsterTattoo 15"] = {"FM_Hip_M_Tat_014",55,""},
		["HipsterTattoo 16"] = {"FM_Hip_M_Tat_015",55,""},
		["HipsterTattoo 17"] = {"FM_Hip_M_Tat_016",55,""},
		["HipsterTattoo 18"] = {"FM_Hip_M_Tat_017",55,""},
		["HipsterTattoo 19"] = {"FM_Hip_M_Tat_018",55,""},
		["HipsterTattoo 20"] = {"FM_Hip_M_Tat_019",55,""},
		["HipsterTattoo 21"] = {"FM_Hip_M_Tat_020",55,""},
		["HipsterTattoo 22"] = {"FM_Hip_M_Tat_021",55,""},
		["HipsterTattoo 23"] = {"FM_Hip_M_Tat_022",55,""},
		["HipsterTattoo 24"] = {"FM_Hip_M_Tat_023",55,""},
		["HipsterTattoo 25"] = {"FM_Hip_M_Tat_024",55,""},
		["HipsterTattoo 26"] = {"FM_Hip_M_Tat_025",55,""},
		["HipsterTattoo 27"] = {"FM_Hip_M_Tat_026",55,""},
		["HipsterTattoo 28"] = {"FM_Hip_M_Tat_027",55,""},
		["HipsterTattoo 29"] = {"FM_Hip_M_Tat_028",55,""},
		["HipsterTattoo 30"] = {"FM_Hip_M_Tat_029",55,""},
		["HipsterTattoo 31"] = {"FM_Hip_M_Tat_030",55,""},
		["HipsterTattoo 32"] = {"FM_Hip_M_Tat_031",55,""},
		["HipsterTattoo 33"] = {"FM_Hip_M_Tat_032",55,""},
		["HipsterTattoo 34"] = {"FM_Hip_M_Tat_033",55,""},
		["HipsterTattoo 35"] = {"FM_Hip_M_Tat_034",55,""},
		["HipsterTattoo 36"] = {"FM_Hip_M_Tat_035",55,""},
		["HipsterTattoo 37"] = {"FM_Hip_M_Tat_036",55,""},
		["HipsterTattoo 38"] = {"FM_Hip_M_Tat_037",55,""},
		["HipsterTattoo 39"] = {"FM_Hip_M_Tat_038",55,""},
		["HipsterTattoo 40"] = {"FM_Hip_M_Tat_039",55,""},
		["HipsterTattoo 41"] = {"FM_Hip_M_Tat_040",55,""},
		["HipsterTattoo 42"] = {"FM_Hip_M_Tat_041",55,""},
		["HipsterTattoo 43"] = {"FM_Hip_M_Tat_042",55,""},
		["HipsterTattoo 44"] = {"FM_Hip_M_Tat_043",55,""}, 
		["HipsterTattoo 45"] = {"FM_Hip_M_Tat_044",55,""},
		["HipsterTattoo 46"] = {"FM_Hip_M_Tat_045",55,""},
		["HipsterTattoo 47"] = {"FM_Hip_M_Tat_046",55,""},
		["HipsterTattoo 48"] = {"FM_Hip_M_Tat_047",55,""},
		["HipsterTattoo 49"] = {"FM_Hip_M_Tat_048",55,""}
	},
}

-- list of tattooshops positions
cfg.shops = {
  {"mpbeach_overlays", 1322.645,-1651.976,52.275},
  {"mpbeach_overlays", -1153.676,-1425.68,4.954},
  {"mpbusiness_overlays", 322.139,180.467,103.587},
  {"mpbusiness_overlays", -3170.071,1075.059,20.829},
  {"mphipster_overlays", 1864.633,3747.738,33.032},
  {"mphipster_overlays", -293.713,6200.04,31.487}
}

return cfg

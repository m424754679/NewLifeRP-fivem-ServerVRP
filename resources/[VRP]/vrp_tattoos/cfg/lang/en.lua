
local lang = {
	tattoos = {
		title = "Tattoos",
		added = "~g~Tattoo added.",
		removed = "~r~Tattoo removed.",
		cleaned = "~r~All tattoos removed."
	},
}

return lang

local lang = {
	tattoos = {
		title = "Tatuagens",
		added = "~g~Tatuagem adicionada.",
		removed = "~r~Tatuagem removida.",
		cleaned = "~r~Todas as tatuagens removidas."
	},
}

return lang
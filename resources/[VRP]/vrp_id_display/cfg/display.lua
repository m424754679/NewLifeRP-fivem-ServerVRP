cfg = {}

cfg.showself = false -- True: shows your own id and blip
cfg.distance = 7 -- Max distance for id

cfg.default = {r = 255, g = 255, b = 255} -- Colors for default id
cfg.talker = {r = 255, g = 255, b = 51} -- Colors for talker id

cfg.showteam = false -- True: shows team colored id to everyone, not just team members
cfg.hideteam = true -- True: hides team colored id to everyone, make them use the cfg.default color for id

cfg.blips = { -- Groups blip display 
  ["[Recruta] - Polícia Militar"] = {
    id = {r = 70, g = 100, b = 200}, -- Colors for group id and vrp_cmd team chat color
	sprite = 1, -- Sprite for group blip
	colour = 29, -- Colour for group blip
	distance = 100 -- Max distance for group blip
  },
  ["[Soldado] - Polícia Militar"] = {
    id = {r = 70, g = 100, b = 200}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 29, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Cabo] - Polícia Militar"] = {
    id = {r = 70, g = 100, b = 200}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 29, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[3° Sargento] - Polícia Militar"] = {
    id = {r = 70, g = 100, b = 200}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 29, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[2° Sargento] - Polícia Militar"] = {
    id = {r = 70, g = 100, b = 200}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 29, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[1° Sargento] - Polícia Militar"] = {
    id = {r = 70, g = 100, b = 200}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 29, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Sub.Tenente] - Polícia Militar"] = {
    id = {r = 70, g = 100, b = 200}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 29, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[2° Tenente] - Polícia Militar"] = {
    id = {r = 70, g = 100, b = 200}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 29, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[1° Tenente] - Polícia Militar"] = {
    id = {r = 70, g = 100, b = 200}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 29, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Capitão] - Polícia Militar"] = {
    id = {r = 70, g = 100, b = 200}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 29, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Major] - Polícia Militar"] = {
    id = {r = 70, g = 100, b = 200}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 29, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Recruta] - ÁGUIA"] = {
    id = {r = 0, g = 80, b = 118}, -- Colors for group id and vrp_cmd team chat color
  sprite = 43, -- Sprite for group blip
  colour = 49, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Soldado] - ÁGUIA"] = {
    id = {r = 0, g = 80, b = 118}, -- Colors for group id and vrp_cmd team chat color
  sprite = 43, -- Sprite for group blip
  colour = 49, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Cabo] - ÁGUIA"] = {
    id = {r = 0, g = 80, b = 118}, -- Colors for group id and vrp_cmd team chat color
  sprite = 43, -- Sprite for group blip
  colour = 49, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[3° Sargento] - ÁGUIA"] = {
    id = {r = 0, g = 80, b = 118}, -- Colors for group id and vrp_cmd team chat color
  sprite = 43, -- Sprite for group blip
  colour = 49, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[2° Sargento] - ÁGUIA"] = {
    id = {r = 0, g = 80, b = 118}, -- Colors for group id and vrp_cmd team chat color
  sprite = 43, -- Sprite for group blip
  colour = 49, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[1° Sargento] - ÁGUIA"] = {
    id = {r = 0, g = 80, b = 118}, -- Colors for group id and vrp_cmd team chat color
  sprite = 43, -- Sprite for group blip
  colour = 49, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Sub.Tenente] - ÁGUIA"] = {
    id = {r = 0, g = 80, b = 118}, -- Colors for group id and vrp_cmd team chat color
  sprite = 43, -- Sprite for group blip
  colour = 49, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[2° Tenente] - ÁGUIA"] = {
    id = {r = 0, g = 80, b = 118}, -- Colors for group id and vrp_cmd team chat color
  sprite = 43, -- Sprite for group blip
  colour = 49, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[1° Tenente] - ÁGUIA"] = {
    id = {r = 0, g = 80, b = 118}, -- Colors for group id and vrp_cmd team chat color
  sprite = 43, -- Sprite for group blip
  colour = 49, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Capitão] - ÁGUIA"] = {
    id = {r = 0, g = 80, b = 118}, -- Colors for group id and vrp_cmd team chat color
  sprite = 43, -- Sprite for group blip
  colour = 49, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Major] - ÁGUIA"] = {
    id = {r = 0, g = 80, b = 118}, -- Colors for group id and vrp_cmd team chat color
  sprite = 43, -- Sprite for group blip
  colour = 49, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Recruta] - ROCAM"] = {
    id = {r = 0, g = 0, b = 0}, -- Colors for group id and vrp_cmd team chat color
  sprite = 512, -- Sprite for group blip
  colour = 40, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Soldado] - ROCAM"] = {
    id = {r = 0, g = 0, b = 0}, -- Colors for group id and vrp_cmd team chat color
  sprite = 512, -- Sprite for group blip
  colour = 40, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Cabo] - ROCAM"] = {
    id = {r = 0, g = 0, b = 0}, -- Colors for group id and vrp_cmd team chat color
  sprite = 512, -- Sprite for group blip
  colour = 40, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[3° Sargento] - ROCAM"] = {
    id = {r = 0, g = 0, b = 0}, -- Colors for group id and vrp_cmd team chat color
  sprite = 512, -- Sprite for group blip
  colour = 40, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[2° Sargento] - ROCAM"] = {
    id = {r = 0, g = 0, b = 0}, -- Colors for group id and vrp_cmd team chat color
  sprite = 512, -- Sprite for group blip
  colour = 40, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[1° Sargento] - ROCAM"] = {
    id = {r = 0, g = 0, b = 0}, -- Colors for group id and vrp_cmd team chat color
  sprite = 512, -- Sprite for group blip
  colour = 40, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Sub.Tenente] - ROCAM"] = {
    id = {r = 0, g = 0, b = 0}, -- Colors for group id and vrp_cmd team chat color
  sprite = 512, -- Sprite for group blip
  colour = 40, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[2° Tenente] - ROCAM"] = {
    id = {r = 0, g = 0, b = 0}, -- Colors for group id and vrp_cmd team chat color
  sprite = 512, -- Sprite for group blip
  colour = 40, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[1° Tenente] - ROCAM"] = {
    id = {r = 0, g = 0, b = 0}, -- Colors for group id and vrp_cmd team chat color
  sprite = 512, -- Sprite for group blip
  colour = 40, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Capitão] - ROCAM"] = {
    id = {r = 0, g = 0, b = 0}, -- Colors for group id and vrp_cmd team chat color
  sprite = 512, -- Sprite for group blip
  colour = 40, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Major] - ROCAM"] = {
    id = {r = 0, g = 0, b = 0}, -- Colors for group id and vrp_cmd team chat color
  sprite = 512, -- Sprite for group blip
  colour = 40, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Recruta] - Força Tática"] = {
    id = {r = 0, g = 0, b = 255}, -- Colors for group id and vrp_cmd team chat color
  sprite = 60, -- Sprite for group blip
  colour = 49, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Soldado] - Força Tática"] = {
    id = {r = 0, g = 0, b = 255}, -- Colors for group id and vrp_cmd team chat color
  sprite = 60, -- Sprite for group blip
  colour = 49, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Cabo] - Força Tática"] = {
    id = {r = 0, g = 0, b = 255}, -- Colors for group id and vrp_cmd team chat color
  sprite = 60, -- Sprite for group blip
  colour = 49, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[3° Sargento] - Força Tática"] = {
    id = {r = 0, g = 0, b = 255}, -- Colors for group id and vrp_cmd team chat color
  sprite = 60, -- Sprite for group blip
  colour = 49, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[2° Sargento] - Força Tática"] = {
    id = {r = 0, g = 0, b = 255}, -- Colors for group id and vrp_cmd team chat color
  sprite = 60, -- Sprite for group blip
  colour = 49, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[1° Sargento] - Força Tática"] = {
    id = {r = 0, g = 0, b = 255}, -- Colors for group id and vrp_cmd team chat color
  sprite = 60, -- Sprite for group blip
  colour = 49, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Sub.Tenente] - Força Tática"] = {
    id = {r = 0, g = 0, b = 255}, -- Colors for group id and vrp_cmd team chat color
  sprite = 60, -- Sprite for group blip
  colour = 49, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[2° Tenente] - Força Tática"] = {
    id = {r = 0, g = 0, b = 255}, -- Colors for group id and vrp_cmd team chat color
  sprite = 60, -- Sprite for group blip
  colour = 49, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[1° Tenente] - Força Tática"] = {
    id = {r = 0, g = 0, b = 255}, -- Colors for group id and vrp_cmd team chat color
  sprite = 60, -- Sprite for group blip
  colour = 49, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Capitão] - Força Tática"] = {
    id = {r = 0, g = 0, b = 255}, -- Colors for group id and vrp_cmd team chat color
  sprite = 60, -- Sprite for group blip
  colour = 49, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Major] - Força Tática"] = {
    id = {r = 0, g = 0, b = 255}, -- Colors for group id and vrp_cmd team chat color
  sprite = 60, -- Sprite for group blip
  colour = 49, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Sub.Tenente] - Força Tática"] = {
    id = {r = 0, g = 0, b = 255}, -- Colors for group id and vrp_cmd team chat color
  sprite = 60, -- Sprite for group blip
  colour = 49, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Recruta] - ROTA"] = {
    id = {r = 73, g = 75, b = 255}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 38, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },  
  ["[Soldado] - ROTA"] = {
    id = {r = 73, g = 75, b = 255}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 38, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  }, 
  ["[Cabo] - ROTA"] = {
    id = {r = 73, g = 75, b = 255}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 38, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  }, 
  ["[3° Sargento] - ROTA"] = {
    id = {r = 73, g = 75, b = 255}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 38, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  }, 
  ["[2° Sargento] - ROTA"] = {
    id = {r = 73, g = 75, b = 255}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 38, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  }, 
  ["[1° Sargento] - ROTA"] = {
    id = {r = 73, g = 75, b = 255}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 38, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  }, 
  ["[Sub.Tenente] - ROTA"] = {
    id = {r = 73, g = 75, b = 255}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 38, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  }, 
  ["[2° Tenente] - ROTA"] = {
    id = {r = 73, g = 75, b = 255}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 38, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  }, 
  ["[1° Tenente] - ROTA"] = {
    id = {r = 73, g = 75, b = 255}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 38, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  }, 
  ["[Capitão] - ROTA"] = {
    id = {r = 73, g = 75, b = 255}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 38, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  }, 
  ["[Major] - ROTA"] = {
    id = {r = 73, g = 75, b = 255}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 38, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  }, 
  ["[Recruta] - Corpo de Bombeiros"] = {
    id = {r = 255, g = 0, b = 0}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 75, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },  
  ["[Soldado] - Corpo de Bombeiros"] = {
    id = {r = 255, g = 0, b = 0}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 75, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },  
  ["[Cabo] - Corpo de Bombeiros"] = {
    id = {r = 255, g = 0, b = 0}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 75, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },  
  ["[3° Sargento] - Corpo de Bombeiros"] = {
    id = {r = 255, g = 0, b = 0}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 75, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },  
  ["[2° Sargento] - Corpo de Bombeiros"] = {
    id = {r = 255, g = 0, b = 0}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 75, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },  
  ["[1° Sargento] - Corpo de Bombeiros"] = {
    id = {r = 255, g = 0, b = 0}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 75, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },  
  ["[Sub.Tenente] - Corpo de Bombeiros"] = {
    id = {r = 255, g = 0, b = 0}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 75, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },  
  ["[2° Tenente] - Corpo de Bombeiros"] = {
    id = {r = 255, g = 0, b = 0}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 75, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },  
  ["[1° Tenente] - Corpo de Bombeiros"] = {
    id = {r = 255, g = 0, b = 0}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 75, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },  
  ["[Capitão] - Corpo de Bombeiros"] = {
    id = {r = 255, g = 0, b = 0}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 75, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },  
  ["[Major] - Corpo de Bombeiros"] = {
    id = {r = 255, g = 0, b = 0}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 75, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },  
  ["[Agente] - Polícia Civil"] = {
    id = {r = 3, g = 222, b = 245}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 29, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Agente] - Polícia Civil"] = {
    id = {r = 3, g = 222, b = 245}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 29, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Agente] - Polícia Civil"] = {
    id = {r = 3, g = 222, b = 245}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 29, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Agente] - Polícia Civil"] = {
    id = {r = 3, g = 222, b = 245}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 29, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Agente Especial] - Polícia Civil"] = {
    id = {r = 3, g = 222, b = 245}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 29, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Escrivão] - Polícia Civil"] = {
    id = {r = 3, g = 222, b = 245}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 29, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Perito] - Polícia Civil"] = {
    id = {r = 3, g = 222, b = 245}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 29, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Atirador de Elite] - Polícia Civil"] = {
    id = {r = 3, g = 222, b = 245}, -- Colors for group id and vrp_cmd team chat color
  sprite = 1, -- Sprite for group blip
  colour = 29, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Tenente.Coronel] - Polícia Militar"] = {
    id = {r = 3, g = 222, b = 245}, -- Colors for group id and vrp_cmd team chat color
  sprite = 304, -- Sprite for group blip
  colour = 26, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Coronel] - Polícia Militar"] = {
    id = {r = 3, g = 222, b = 245}, -- Colors for group id and vrp_cmd team chat color
  sprite = 304, -- Sprite for group blip
  colour = 38, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Coronel] - Força Tática"] = {
    id = {r = 3, g = 222, b = 245}, -- Colors for group id and vrp_cmd team chat color
  sprite = 304, -- Sprite for group blip
  colour = 63, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Tenente.Coronel] - ROTA"] = {
    id = {r = 3, g = 222, b = 245}, -- Colors for group id and vrp_cmd team chat color
  sprite = 304, -- Sprite for group blip
  colour = 39, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Coronel] - ROTA"] = {
    id = {r = 3, g = 222, b = 245}, -- Colors for group id and vrp_cmd team chat color
  sprite = 304, -- Sprite for group blip
  colour = 40, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Tenente.Coronel] - Corpo de Bombeiros"] = {
    id = {r = 3, g = 222, b = 245}, -- Colors for group id and vrp_cmd team chat color
  sprite = 304, -- Sprite for group blip
  colour = 49, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
  ["[Coronel] - Corpo de Bombeiros"] = {
    id = {r = 3, g = 222, b = 245}, -- Colors for group id and vrp_cmd team chat color
  sprite = 304, -- Sprite for group blip
  colour = 59, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  }, 
  ["[Delegado] Policia Civil"] = {
    id = {r = 3, g = 222, b = 245}, -- Colors for group id and vrp_cmd team chat color
  sprite = 304, -- Sprite for group blip
  colour = 28, -- Colour for group blip
  distance = 100 -- Max distance for group blip
  },
}

cfg.teams = { -- groups on the same teams can see each other (in this case sheriff can see both and be seen by both but they cant see each other)
  ["POLICIA"] = { -- Team name
    -- groups
    "[Recruta] - Polícia Militar",
    "[Soldado] - Polícia Militar",
    "[Cabo] - Polícia Militar",
    "[3° Sargento] - Polícia Militar",
    "[2° Sargento] - Polícia Militar",
    "[1° Sargento] - Polícia Militar",
    "[Sub.Tenente] - Polícia Militar",
    "[2° Tenente] - Polícia Militar",
    "[1° Tenente] - Polícia Militar",
    "[Capitão] - Polícia Militar",
    "[Major] - Polícia Militar",
    "[Recruta] - ÁGUIA" ,
    "[Soldado] - ÁGUIA",
    "[Cabo] - ÁGUIA",
    "[3° Sargento] - ÁGUIA",
    "[2° Sargento] - ÁGUIA",
    "[1° Sargento] - ÁGUIA",
    "[Sub.Tenente] - ÁGUIA",
    "[2° Tenente] - ÁGUIA",
    "[1° Tenente] - ÁGUIA",
    "[Capitão] - ÁGUIA",
    "[Major] - ÁGUIA",
    "[Recruta] - ROCAM",
    "[Soldado] - ROCAM",
    "[Cabo] - ROCAM",
    "[3° Sargento] - ROCAM",
    "[2° Sargento] - ROCAM",
    "[1° Sargento] - ROCAM",
    "[Sub.Tenente] - ROCAM",
    "[2° Tenente] - ROCAM",
    "[1° Tenente] - ROCAM",
    "[Capitão] - ROCAM",
    "[Major] - ROCAM",
    "[Recruta] - Força Tática",
    "[Soldado] - Força Tática",
    "[Cabo] - Força Tática",
    "[3° Sargento] - Força Tática",
    "[2° Sargento] - Força Tática",
    "[1° Sargento] - Força Tática",
    "[Sub.Tenente] - Força Tática",
    "[2° Tenente] - Força Tática",
    "[1° Tenente] - Força Tática",
    "[Capitão] - Força Tática",
    "[Major] - Força Tática",
    "[Sub.Tenente] - Força Tática",
    "[Recruta] - ROTA",
    "[Soldado] - ROTA",
    "[Cabo] - ROTA",
    "[3° Sargento] - ROTA",
    "[2° Sargento] - ROTA",
    "[1° Sargento] - ROTA",
    "[Sub.Tenente] - ROTA",
    "[2° Tenente] - ROTA",
    "[1° Tenente] - ROTA",
    "[Capitão] - ROTA",
    "[Major] - ROTA",
    "[Recruta] - Corpo de Bombeiros",
    "[Soldado] - Corpo de Bombeiros",
    "[Cabo] - Corpo de Bombeiros",
    "[3° Sargento] - Corpo de Bombeiros",
    "[2° Sargento] - Corpo de Bombeiros",
    "[1° Sargento] - Corpo de Bombeiros",
    "[Sub.Tenente] - Corpo de Bombeiros",
    "[2° Tenente] - Corpo de Bombeiros",
    "[1° Tenente] - Corpo de Bombeiros",
    "[Capitão] - Corpo de Bombeiros",
    "[Major] - Corpo de Bombeiros",
    "[Agente] - Polícia Civil",
    "[Agente] - Polícia Civil",
    "[Agente] - Polícia Civil",
    "[Agente] - Polícia Civil",
    "[Agente Especial] - Polícia Civil",
    "[Escrivão] - Polícia Civil",
    "[Perito] - Polícia Civil",
    "[Atirador de Elite] - Polícia Civil",
    "[Tenente.Coronel] - Polícia Militar",
    "[Coronel] - Polícia Militar",
    "[Coronel] - Força Tática",
    "[Tenente.Coronel] - ROTA",
    "[Coronel] - ROTA",
    "[Tenente.Coronel] - Corpo de Bombeiros",
    "[Coronel] - Corpo de Bombeiros",
    "[Delegado] Policia Civil"
  },
  ["EMS"] = { -- Team name
    -- groups
    "emergency",
    "sheriff"
  },
  -- create more teams here
}

return cfg
-- Link for blip colours: http://i.imgur.com/Hvyx6cE.png
-- Link for blip sprites: https://marekkraus.sk/gtav/blips/list.html

local cfg = {}

--[[
TO MAKE A DRUG ADD IT TO cfg.drugs LIKE SO:
  ["itemid"] = {
    name = "Name", -- item name
    desc = "Some description.", -- item description
    choices = function(args) -- create choices
	  local menu = {} -- creates menu
      menu["Choice"] = {function(player,choice) -- menu choice function
  	    local user_id = vRP.getUserId({player}) -- get user id
  	    if user_id ~= nil then -- check user_id not nil
  	      if vRP.tryGetInventoryItem({user_id,"itemid",1,false}) then -- get item
            -- vRP.varyHunger({user_id,30}) -- optional
            -- vRP.varyThirst({user_id,-70}) -- optional
  		    -- vRPclient.varyHealth(player,{10}) -- optional
  		    vRPclient.notify(player,{"~g~Smoking weed."}) -- notify use
  		    local seq = { -- this should be the sequence of animation
  		    }
  		    vRPclient.playAnim(player,{true,seq,false}) -- play animation sequence
  		    SetTimeout(10000,function() -- here a timeout to for effect to start
  		      Dclient.playMovement(player,{"MOVE_M@DRUNK@SLIGHTLYDRUNK",true,true,false,false}) -- start movement effect, check client.lua for info about the function
  		      Dclient.playScreenEffect(player, {"DMT_flight", 120}) -- start screen effect, check client.lua for info about the function
  		    end)
  		    SetTimeout(120000,function() -- here a timeout for movement effect to end, screen effect has timer on function
  			  Dclient.resetMovement(player,{false}) -- stop movement effect
  		    end)
  		    vRP.closeMenu({player}) -- close menu
  		  end
  	    end
  	  end}
	  return menu -- return choices
    end,
	weight = 0.1 -- item weight
  },
]]

cfg.drugs= {
  ["cannabis"] = {
    name = "Maconha",
    desc = "Alguns baseados.",
    choices = function(args)
    local menu = {}
      menu["Fumar"] = {function(player,choice)
        local user_id = vRP.getUserId(player)
        if user_id ~= nil then
          if vRP.tryGetInventoryItem(user_id,"maconha",1,false) then
          vRPclient.varyHealth(player,25)
          vRPclient.notify(player,"~g~Fumando maconha.")
          local seq = {
            {"mp_player_int_uppersmoke","mp_player_int_smoke_enter",1},
            {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
            {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
            {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
            {"mp_player_int_uppersmoke","mp_player_int_smoke_exit",1}
          }
          vRPclient.playAnim(player,true,seq,false)
          SetTimeout(10000,function()
            Dclient.playMovement(player,"MOVE_M@DRUNK@SLIGHTLYDRUNK",true,true,false,false)
            Dclient.playScreenEffect(player, "DMT_flight", 120)
          end)
          SetTimeout(120000,function()
          Dclient.resetMovement(player,false)
          end)
          vRP.closeMenu(player)
        end
        end
      end}
    return menu
    end,
  weight = 0.1
  },
  ["cocaina"] = {
    name = "cocaina",
    desc = "Cheirar cocaina.",
    choices = function(args)
    local menu = {}
      menu["cheirar"] = {function(player,choice)
        local user_id = vRP.getUserId(player)
        if user_id ~= nil then
          if vRP.tryGetInventoryItem(user_id,"cocaina",1,false) then
          vRPclient.varyHealth(player,-25)
          vRPclient.notify(player,"~g~Cheirando cocaina.")
          local seq = {
            {"mp_player_int_uppersmoke","mp_player_int_smoke_enter",1},
            {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
            {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
            {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
            {"mp_player_int_uppersmoke","mp_player_int_smoke_exit",1}
          }
          vRPclient.playAnim(player,true,seq,false)
          SetTimeout(10000,function()
            Dclient.playMovement(player,"MOVE_M@DRUNK@SLIGHTLYDRUNK",true,true,false,false)
            Dclient.playScreenEffect(player, "DMT_flight", 120)
          end)
          SetTimeout(120000,function()
          Dclient.resetMovement(player,false)
          end)
          vRP.closeMenu(player)
        end
        end
      end}
    return menu
    end,
   weight = 1.0
  },
  ["metanfetamina"] = {
    name = "Metafetaminaa",
    desc = "injetar um pouco de Metafetamina.",
    choices = function(args)
    local menu = {}
      menu["Injetar"] = {function(player,choice)
        local user_id = vRP.getUserId(player)
        if user_id ~= nil then
          if vRP.tryGetInventoryItem(user_id,"metanfetamina",1,false) then
          vRPclient.varyHealth(player,-25)
          vRPclient.notify(player,"~g~Injetando metafetamina.")
          local seq = {
            {"mp_player_int_uppersmoke","mp_player_int_smoke_enter",1},
            {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
            {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
            {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
            {"mp_player_int_uppersmoke","mp_player_int_smoke_exit",1}
          }
          vRPclient.playAnim(player,true,seq,false)
          SetTimeout(10000,function()
            Dclient.playMovement(player,"MOVE_M@DRUNK@SLIGHTLYDRUNK",true,true,false,false)
            Dclient.playScreenEffect(player, "DMT_flight", 120)
          end)
          SetTimeout(120000,function()
          Dclient.resetMovement(player,false)
          end)
          vRP.closeMenu(player)
        end
        end
      end}
    return menu
    end,
   weight = 1.0
  },
  ["heroina"] = {
    name = "Heroina",
    desc = "Injetar heroina.",
    choices = function(args)
    local menu = {}
      menu["injeta"] = {function(player,choice)
        local user_id = vRP.getUserId(player)
        if user_id ~= nil then
          if vRP.tryGetInventoryItem(user_id,"heroina",1,false) then
          vRPclient.varyHealth(player,-25)
          vRPclient.notify(player,"~g~Injetando heroina.")
          local seq = {
            {"mp_player_int_uppersmoke","mp_player_int_smoke_enter",1},
            {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
            {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
            {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
            {"mp_player_int_uppersmoke","mp_player_int_smoke_exit",1}
          }
          vRPclient.playAnim(player,true,seq,false)
          SetTimeout(10000,function()
            Dclient.playMovement(player,"MOVE_M@DRUNK@SLIGHTLYDRUNK",true,true,false,false)
            Dclient.playScreenEffect(player, "DMT_flight", 120)
          end)
          SetTimeout(120000,function()
          Dclient.resetMovement(player,false)
          end)
          vRP.closeMenu(player)
        end
        end
      end}
    return menu
    end,
   weight = 1.0
  },
  ["crack"] = {
    name = "Crack",
    desc = "Usar crack na latinha de Cristal.",
    choices = function(args)
    local menu = {}
      menu["Fumar"] = {function(player,choice)
        local user_id = vRP.getUserId(player)
        if user_id ~= nil then
          if vRP.tryGetInventoryItem(user_id,"crack",1,false) then
          vRPclient.varyHealth(player,-25)
          vRPclient.notify(player,"~g~Usando crack.")
          local seq = {
            {"mp_player_int_uppersmoke","mp_player_int_smoke_enter",1},
            {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
            {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
            {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
            {"mp_player_int_uppersmoke","mp_player_int_smoke_exit",1}
          }
          vRPclient.playAnim(player,true,seq,false)
          SetTimeout(10000,function()
            Dclient.playMovement(player,"MOVE_M@DRUNK@SLIGHTLYDRUNK",true,true,false,false)
            Dclient.playScreenEffect(player, "DMT_flight", 120)
          end)
          SetTimeout(120000,function()
          Dclient.resetMovement(player,false)
          end)
          vRP.closeMenu(player)
        end
        end
      end}
    return menu
    end,
   weight = 1.0
  },
  ["lanca"] = {
    name = "Lança Perfume",
    desc = "Baforar um pouco do lança.",
    choices = function(args)
    local menu = {}
      menu["Baforar"] = {function(player,choice)
        local user_id = vRP.getUserId(player)
        if user_id ~= nil then
          if vRP.tryGetInventoryItem(user_id,"lanca",1,false) then
          vRPclient.varyHealth(player,-25)
          vRPclient.notify(player,"~g~baforando o lança.")
          local seq = {
            {"mp_player_int_uppersmoke","mp_player_int_smoke_enter",1},
            {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
            {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
            {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
            {"mp_player_int_uppersmoke","mp_player_int_smoke_exit",1}
          }
          vRPclient.playAnim(player,true,seq,false)
          SetTimeout(10000,function()
            Dclient.playMovement(player,"MOVE_M@DRUNK@SLIGHTLYDRUNK",true,true,false,false)
            Dclient.playScreenEffect(player, "DMT_flight", 120)
          end)
          SetTimeout(120000,function()
          Dclient.resetMovement(player,false)
          end)
          vRP.closeMenu(player)
        end
        end
      end}
    return menu
    end,
   weight = 1.0
  },
  ["sighmirnoff"] = {
    name = "Sighmirnoff",
    desc = "Very strong vodka.",
    choices = function(args)
	  local menu = {}
      menu["Drink"] = {function(player,choice)
        local user_id = vRP.getUserId(player)
        if user_id ~= nil then
          if vRP.tryGetInventoryItem(user_id,"sighmirnoff",1,false) then
            vRP.varyHunger(user_id,30)
            vRP.varyThirst(user_id,-70)
  		    vRPclient.varyHealth(player,10)

            vRPclient.notify(player,"~b~Drinking Sighmirnoff.")
            local seq = {
              {"mp_player_intdrink","intro_bottle",1},
              {"mp_player_intdrink","loop_bottle",1},
              {"mp_player_intdrink","outro_bottle",1}
            }
            vRPclient.playAnim(player,true,seq,false)
			SetTimeout(5000,function()
			  Dclient.playMovement(player,"MOVE_M@DRUNK@VERYDRUNK",true,true,false,false)
			  Dclient.playScreenEffect(player, "Rampage", 120)
			  SetTimeout(120000,function()
			    Dclient.resetMovement(player,false)
			  end)
			end)
            vRP.closeMenu(player)
          end
        end
      end}
	  return menu
    end,
	weight = 0.5
  },
}

return cfg

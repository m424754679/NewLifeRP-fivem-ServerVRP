-- Some messages might not work because I thought Luang worked client-side
-- I still think it might work in the future so I will just comment those messages for now
-- These commented messages can be changed on the files inside the cfg/client folder
local lang = {
	barbershop = {
		title = "Barbershop",
		button = "@Show Overlay",
		perm = "admin.overlay",
		desc = "Show ped overlay."
	},
}

return lang